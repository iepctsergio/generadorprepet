﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Prepet.Conexion;
using Prepet.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Prepet
{
    public class MainCSV
    {
        private string ProcesoOrdinario = "ORD-2018";      
        private int idAmbito= 0;
        private DateTime DateTimeFecha;
        private string Fecha;
        public string CreateCSVFile(DataTable gubernatura, DataTable diputacion, DataTable municipal, string[] FechaPublicacion,bool ultimo_corte) {

            var HoraCorte = FechaPublicacion[0];
            var array = FechaPublicacion[1].Split('/');
            Fecha = FechaPublicacion[1];
            DateTimeFecha = new DateTime(int.Parse(array[2]), int.Parse(array[1]), int.Parse(array[0]));
            Thread thread1 = new Thread(() => getCSVMunicipal(municipal, HoraCorte, ultimo_corte));

            Thread thread2 = new Thread(() => getCSVDiputaciones(diputacion, HoraCorte, ultimo_corte));

            Thread thread3 = new Thread(() => getCSVGobernatura(gubernatura,HoraCorte, ultimo_corte));

            thread1.Start();
            thread2.Start();
            thread3.Start();

            while (thread1.IsAlive || thread2.IsAlive || thread3.IsAlive)
            {

            }
            return TarGz(HoraCorte);
        }
        public void CSVGOBCandidatos()
        {
            var lista =new ConexionBD().IEnumerableCollectionData("call PREPET_Lista_Candidatos_Y_Suplentes('"+ProcesoOrdinario+"')");
            var resultado = from row in lista
                            where ((row["NOMBRE_ELECCION"]+"")== "GUBERNATURA")
                            select new
                            {
                                ESTADO = 27,
                                DISTRITO = 0,
                                MUNICIPIO = 0,
                                PARTIDO =row["PARTIDO"],
                                CANDIDATO_PROPIETARIO = row["NOMBRE_PROPIETARIO"],
                                CANDIDATO_SUPLENTE = row["NOMBRE_SUPLENTE"]
                            };
            var prop=resultado.FirstOrDefault().GetType().GetProperties();
            var csv = String.Join(",", prop.Select(f => f.Name).ToArray());
            csv += "\r\n";
            foreach (var item in resultado)
            {
                csv += item.ESTADO + ",";
                csv += item.DISTRITO + ",";
                csv += item.MUNICIPIO + ",";
                csv += item.PARTIDO + ",";
                csv += item.CANDIDATO_PROPIETARIO+ ",";
                csv += item.CANDIDATO_SUPLENTE;
                csv += "\r\n";
            }
            string directory = @"c:\Json\Informacion\GUBERNATURA\";
            string directorySave = directory + "TAB_GOB_CANDIDATOS_" + DateTimeFecha.Year+".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csv);
            }
            fs.Dispose();
            fs.Close();
            lista = null;
            resultado = null;
            prop = null;
            csv = null;
        }
        public void CSVDIP_LOCALCandidatos()
        {

            var lista = new ConexionBD().IEnumerableCollectionData("call PREPET_Lista_Candidatos_Y_Suplentes('"+ProcesoOrdinario+"')");
            var resultado = from row in lista
                            where ((row["NOMBRE_ELECCION"] + "") == "DIPUTACIONES")
                            select new
                            {
                                ESTADO = 27,
                                DISTRITO = row["NUMERO_AMBITO"],
                                MUNICIPIO = 0,
                                PARTIDO = row["PARTIDO"],
                                CANDIDATO_PROPIETARIO = row["NOMBRE_PROPIETARIO"],
                                CANDIDATO_SUPLENTE = row["NOMBRE_SUPLENTE"]
                            };
            var prop = resultado.FirstOrDefault().GetType().GetProperties();
            var csv = String.Join(",", prop.Select(f => f.Name).ToArray());
            csv += "\r\n";
            foreach (var item in resultado)
            {
                csv += item.ESTADO + ",";
                csv += item.DISTRITO + ",";
                csv += item.MUNICIPIO + ",";
                csv += item.PARTIDO + ",";
                csv += item.CANDIDATO_PROPIETARIO + ",";
                csv += item.CANDIDATO_SUPLENTE;
                csv += "\r\n";
            }
            string directory = @"c:\Json\Informacion\DIPUTACIONES\";
            string directorySave = directory + "TAB_DIP_LOC_CANDIDATOS_" + DateTimeFecha.Year+".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csv);
            }
            fs.Dispose();
            fs.Close();
            lista = null;
            resultado = null;
            prop = null;
            csv = null;
        }
        public void CSVMUNCandidatos()
        {

            var lista = new ConexionBD().IEnumerableCollectionData("call PREPET_Lista_Candidatos_Y_Suplentes('"+ProcesoOrdinario+"')");
            var resultado = from row in lista
                            where ((row["NOMBRE_ELECCION"] + "") == "PRESIDENCIAS MUNICIPALES Y REGIDURÍAS")
                            select new
                            {
                                ESTADO = 27,
                                DISTRITO = 0,
                                MUNICIPIO = row["NUMERO_AMBITO"],
                                PARTIDO = row["PARTIDO"],
                                CANDIDATO_PROPIETARIO = row["NOMBRE_PROPIETARIO"],
                                CANDIDATO_SUPLENTE = row["NOMBRE_SUPLENTE"]
                            };
            var prop = resultado.FirstOrDefault().GetType().GetProperties();
            var csv = String.Join(",", prop.Select(f => f.Name).ToArray());
            csv += "\r\n";
            foreach (var item in resultado)
            {
                csv += item.ESTADO + ",";
                csv += item.DISTRITO + ",";
                csv += item.MUNICIPIO + ",";
                csv += item.PARTIDO + ",";
                csv += item.CANDIDATO_PROPIETARIO + ",";
                csv += item.CANDIDATO_SUPLENTE;
                csv += "\r\n";
            }
            string directory = @"c:\Json\Informacion\MUNICIPAL\";
            string directorySave = directory + "TAB_AYUN_CANDIDATOS_" + DateTimeFecha.Year + ".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csv);
            }
            fs.Dispose();
            fs.Close();
            lista = null;
            resultado = null;
            prop = null;
            csv = null;
        }
        public void getCSVMunicipal(DataTable table, string HoraCorte,bool ultimo_corte)
        {
            var info = new Datos.DatosEncabezadoCSV(table);
            var objeto =info.InformacionGenerales(HoraCorte, Fecha,"PRESIDENCIAS MUNICIPALES y REGIDURIAS",ultimo_corte);
            var sa = objeto.GetType().GetProperties().Where(x => x.Name != "timer_update" && x.Name!= "ultimo_corte");
            var csv = string.Join(",", sa.Select(x => x.Name.ToUpper()));
            csv += "\r\n";

            foreach (var property in sa)
            {
                csv += "" + property.GetValue(objeto, null) + ',';
            }
            csv += "\r\n" + "\r\n";
            
            var csvInsert= new ConexionBD().ExportCSV(info.dataTableSeccion, csv, TipoEleccion.Municipales.GetHashCode());

            string directory = @"c:\Json\Informacion\MUNICIPAL\";
            Prepet.zip.ZipCompression.ClearDirectoryFile(new System.IO.DirectoryInfo(directory));
            string directorySave = directory + "TAB_AYUN_" + DateTimeFecha.Year+".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csvInsert);             
            }
            fs.Dispose();
            fs.Close();
            objeto = null;
            sa = null;
            csv = null;
            csvInsert = null;
            CSVMUNCandidatos();
            string leeme = @"c:\Json\LEEME.txt";
            File.Copy(leeme, directory+"LEEME.txt");
        }
        public void getCSVDiputaciones(DataTable table, string HoraCorte,bool ultimo_corte)
        {
            var info = new Datos.DatosEncabezadoCSV(table);
            var objeto = info.InformacionGenerales(HoraCorte, Fecha,"DIPUTACIONES LOCALES",ultimo_corte);
            var sa = objeto.GetType().GetProperties().Where(x => x.Name != "timer_update" && x.Name != "ultimo_corte"); 
            var csv = string.Join(",", sa.Select(x => x.Name.ToUpper()));
            csv += "\r\n";

            foreach (var property in sa)
            {
                csv += "" + property.GetValue(objeto, null) + ',';
            }
            csv += "\r\n" + "\r\n";

            var csvInsert= new ConexionBD().ExportCSV(info.dataTableSeccion, csv, TipoEleccion.Diputaciones.GetHashCode());

            string directory = @"c:\Json\Informacion\DIPUTACIONES\";
            Prepet.zip.ZipCompression.ClearDirectoryFile(new System.IO.DirectoryInfo(directory));
            string directorySave = directory + "TAB_DIP_LOC_"+DateTimeFecha.Year+ ".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csvInsert);
            }
            fs.Dispose();
            fs.Close();
            objeto = null;
            sa = null;
            csv = null;
            csvInsert = null;

            CSVDIP_LOCALCandidatos();

            string leeme = @"c:\Json\LEEME.txt";
            File.Copy(leeme, directory + "LEEME.txt");
        }
        public void getCSVGobernatura(DataTable table, string HoraCorte,bool ultimo_corte)
        {
            var info = new Datos.DatosEncabezadoCSV(table);// ProcesoOrdinario, TipoEleccion.Gubernatura.GetHashCode(), idAmbito);
            var objeto= info.InformacionGenerales(HoraCorte, Fecha,"GUBERNATURA",ultimo_corte);
            var sa = objeto.GetType().GetProperties().Where(x => x.Name != "timer_update" && x.Name != "ultimo_corte");
            var csv = string.Join(",", sa.Select(x => x.Name.ToUpper()));
            csv += "\r\n";

            foreach (var property in sa)
            {
                csv += "" + property.GetValue(objeto, null) + ',';
            }
            csv += "\r\n" + "\r\n";
            var csvInsert= new ConexionBD().ExportCSV(info.dataTableSeccion, csv, TipoEleccion.Gubernatura.GetHashCode());
            string directory = @"c:\Json\Informacion\GUBERNATURA\";
            Prepet.zip.ZipCompression.ClearDirectoryFile(new System.IO.DirectoryInfo(directory));
            string directorySave = directory + "TAB_GOB_" + DateTimeFecha.Year + ".csv";
            System.IO.FileStream fs = File.Create(directorySave);
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(csvInsert);
            }
            fs.Dispose();
            fs.Close();
            objeto = null;
            sa = null;
            csv = null;
            csvInsert = null;

            CSVGOBCandidatos();

            string leeme = @"c:\Json\LEEME.txt";
            File.Copy(leeme, directory + "LEEME.txt");
        }


        public String TarGz(string HoraCorte)
        {
            Prepet.zip.ZipCompression.ClearDirectoryFile(new System.IO.DirectoryInfo(@"c:\Json\Informacion\BASE_DATOS\"));

            var hFile = HoraCorte.Replace(":", "");
            string directorySaveGob = @"c:\Json\Informacion\BASE_DATOS\" + DateTimeFecha.ToString("yyyyMMdd_")+ hFile + "_PREP_GOB_TAB.tar.gz";
            string pathGob = @"c:\Json\Informacion\GUBERNATURA";
            Prepet.zip.ZipCompression.CreateTarGZ(directorySaveGob, pathGob);

            string directorySaveMun = @"c:\Json\Informacion\BASE_DATOS\" + DateTimeFecha.ToString("yyyyMMdd_")+ hFile + "_PREP_AYUN_TAB.tar.gz";
            string pathMun = @"c:\Json\Informacion\MUNICIPAL";
            Prepet.zip.ZipCompression.CreateTarGZ(directorySaveMun, pathMun);

            string directorySaveDiputacion = @"c:\Json\Informacion\BASE_DATOS\" + DateTimeFecha.ToString("yyyyMMdd_")+ hFile + "_PREP_DIP_LOC_TAB.tar.gz";
            string pathDiputacion = @"c:\Json\Informacion\DIPUTACIONES";
            Prepet.zip.ZipCompression.CreateTarGZ(directorySaveDiputacion, pathDiputacion);

            //GUARDA LOS CSV en una ruta donde se tenga acceso 
            var archivo = DateTimeFecha.ToString("yyyyMMdd_")+ hFile + "_BD.tar.gz";
            string directorySave = @"c:\Json\Informacion\" + archivo;
            string directory = @"c:\Json\Informacion\BASE_DATOS";
            Prepet.zip.ZipCompression.CreateTarGZ(directorySave, directory);

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            using (StreamWriter sw = new StreamWriter(@"c:\Json\Data\descarga.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                var x = new descarga()
                {
                    fechaCorte = DateTimeFecha.ToString("dd 'de' MMMM 'de' yyyy"),
                    horaCorte = HoraCorte,
                    basedatos= archivo
                };
                serializer.Serialize(writer, x);
               
            }
            return archivo;
        }
    }

    public class descarga {
        public string fechaCorte { get; set; }
        public string horaCorte { get; set; }
        public string basedatos { get; set; }        
    }

   
}
