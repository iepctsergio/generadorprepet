﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class Ayuntamiento
    {
        public InformacionGeneral informacionGral;
        public AyuntamientoEntidad entidad { get; set; }
        public AyuntamientoDetalleEntidad detalle { get; set;}
        public DetalleMuncipios detalle_municipios { get; set; }
        public ModuloSeccion seccion { get; set; }
    }
}
