﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
   public class gobernatura
    {
        public InformacionGeneral informacionGral { get; set; }
        public GobernaturaEntidad entidad { get; set; }
        public ModuloDetalleEntidad detalleEntidad { get; set; }
        public ModuloDistrito distritos { get; set; }
        public ModuloSeccion seccion { get; set; }
        //public ModuloCasilla casillas { get; set; }
    }
}
