﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class GobernaturaEntidad
    {
        //public InformacionGeneral informacionGral;

        public CandidaturaPartido votosCandidatura;

        public CandidaturaIndependiente votosCandidaturaIndependiente;

        public GobernaturaEntidad() {
            //informacionGral = new InformacionGeneral();
            votosCandidatura = new CandidaturaPartido();
            votosCandidaturaIndependiente = new CandidaturaIndependiente();

        }
    }
}
