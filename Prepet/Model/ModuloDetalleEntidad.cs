﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prepet.Model.Entidad;

namespace Prepet.Model
{
    public class ModuloDetalleEntidad
    {
       // public InformacionGeneral informacionGral;

        public CandidaturaPartido votosCandidatura;

        public CandidaturaIDistrito votosCandidaturaIndependiente;

        public ModuloDetalleEntidad() {
            //informacionGral=new InformacionGeneral();
            votosCandidatura = new CandidaturaPartido();
            votosCandidaturaIndependiente = new CandidaturaIDistrito();
        }          
    }
}
