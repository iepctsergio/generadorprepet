﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class ModuloDistrito
    {
        //public InformacionGeneral informacionGral;

        public CandidaturaPartido votosCandidatura;

        public CandidaturaIDistrito votosCandidaturaIndependiente;

        public ModuloDistrito()
        {
            //informacionGral = new InformacionGeneral();
            votosCandidatura = new CandidaturaPartido();
            votosCandidaturaIndependiente = new CandidaturaIDistrito();

        }
    }
}
