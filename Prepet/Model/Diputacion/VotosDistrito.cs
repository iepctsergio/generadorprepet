﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Diputacion
{
    public class VotosDistrito
    {
        public int id { get; set; }
        public int  distrito { get; set; }
        public decimal total { get; set; }
        public decimal porcentaje { get; set; }
        public string color { get; set; }
        public List<Partidos>  partido { get; set; }
        public List<Distrito> numDistrito { get; set; }           
    }

    public class VotosXPartido {

    }

    public class Distrito
    {
        public int distritoGanado { get; set; }
        public int votosDistrito { get; set; }
    }
}
