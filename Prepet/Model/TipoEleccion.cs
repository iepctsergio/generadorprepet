﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public enum TipoEleccion
    {
        Gubernatura = 2,
        Diputaciones = 1,
        Municipales = 3,
    }
}
