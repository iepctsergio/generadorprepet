﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class ModuloSeccion
    {
        //public InformacionGeneral informacionGral;
        public List<partido>  Partidos { get; set; }
        public IEnumerable<Dictionary<string, object>> Dist_Sec_Casilla { get; set; }
        public List<Seccion> ltSecciones { get; set; }
        public List<ClaseGeneral> ltDistritos { get; set; }
        public object Actas { get; set; }
    }

    public class partido {
        public string columna { get; set; }
        public string logo { get; set; }
    }
}
