﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class Actas
    {
        public int id { get; set; }
        public int id_distrito { get; set; }
        public string  Nombre { get; set; }
        public decimal Esperadas { get; set; }
        public decimal Capturadas { get; set; }
        public decimal Contabilizadas { get; set; }
        public decimal Participacion_Ciudadana { get; set; }
        public decimal TotalListaNominal { get; set; }
    }
}
