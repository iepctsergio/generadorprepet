﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class Data
    {
        public gobernatura gobernatura { get; set; }
        public Diputaciones diputaciones { get; set; }

        public Ayuntamiento ayuntamientos { get; set; }
    }

    public class Resultado {
        public Data prepData { get; set; }
    }
}
