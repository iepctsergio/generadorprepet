﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class Diputaciones
    {
        public InformacionGeneral informacionGralDiputacion { get; set; }
        public InformacionGeneral informacionGralPartidos { get; set; }
        public DiputacionesEntidad entidad { get; set; }
        public DiputacionDetalleEntidad detalle { get; set; }
        public DiputacionDetalleDistrito detalleDistrito { get; set; }
        public ModuloSeccion seccion { get; set; }
        
    }
}
