﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model
{
    public class ModuloCasilla
    {
        //public InformacionGeneral informacionGral;
        public List<Seccion> listaSeccion { get; set; }

        public List<Casilla> listaCasilla { get; set; }

        public List<Actas> listaActas { get; set; }
    }

    public class Casilla {
        public int IdSeccion { get; set; }
        public int  IdCasilla { get; set; }
       public int SeguimientoDigitalizado { get; set; }
       public List<Partidos> partidos { get; set; }
       public int VotosTotales { get; set; }
       public string Inconsistencia { get; set; }
       public int HayInconsistencia { get; set; }
       public string Obervaciones { get; set; }
        public int Orden { get; set; }
       public string CasillaNombre { get; set; }
    }
}
