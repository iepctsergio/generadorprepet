﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class DistribucionVotos
    {
        public string Nombre { get; set; }
        public string Logo { get; set; }
        public decimal Partido { get; set; }
        public decimal Coalicion { get; set; }
        public decimal Total { get { return (Partido + Coalicion); } }
    }

    public class DistribucionVotosXCandidatura
    {
        public string logoPartido { get; set; }
        public string candidato { get; set; }
        public List<Partidos> partido { get; set; }
        public decimal total { get; set; }
        public decimal porcentaje { get; set; }
    }
}
