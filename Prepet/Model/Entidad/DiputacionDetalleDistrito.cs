﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class DiputacionDetalleDistrito
    {
        public InformacionGeneral informacionGral;

        public CandidaturaDetalleDistrito votosCandidatura;
        public DistritoCandidaturaIndependiente votosCandidaturaIndependiente;

        public DiputacionDetalleDistrito()
        {
            votosCandidatura = new CandidaturaDetalleDistrito();
            votosCandidaturaIndependiente = new DistritoCandidaturaIndependiente();
        }
    }


    public class CandidaturaDetalleDistrito {        
        public List<DiputacionDistrito> listaDetalleDistrito { get; set; }

        public CandidaturaDetalleDistrito() {
            listaDetalleDistrito = new List<DiputacionDistrito>();
        }
    }

    public class DistritoCandidaturaIndependiente
    {        
        public List<DiputacionDistrito2> listaDetalleDistrito { get; set; }

        public DistritoCandidaturaIndependiente()
        {
            listaDetalleDistrito = new List<DiputacionDistrito2>();
        }
    }


    public class DiputacionDetalleSeccion
    {
        public List<DiputacionSeccion> listaDetalleDistrito { get; set; }

        public DiputacionDetalleSeccion()
        {
            listaDetalleDistrito = new List<DiputacionSeccion>();
        }
    }

    public class DiputacionDistrito
    {
        public int id { get; set; }
        public string Nombre { get; set; }

        public decimal TotalVotos { get; set; }

        public List<DistribucionVotosXCandidatura> tablaVotosPartido;

        public List<Actas> tablaActasDistrito;

        public DiputacionDistrito() {
            tablaVotosPartido = new List<DistribucionVotosXCandidatura>();
            tablaActasDistrito = new List<Actas>();
        }

    }

    public class DiputacionDistrito2
    {
        public int id { get; set; }
        public string Nombre { get; set; }

        public decimal? TotalVotos { get; set; }

        public List<DistribucionVotos> tablaVotosPartido;

        public List<Actas> tablaActasDistrito;

        public DiputacionDistrito2()
        {
            tablaVotosPartido = new List<DistribucionVotos>();
            tablaActasDistrito = new List<Actas>();
        }

    }

    public class DiputacionSeccion
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public decimal TotalVotos { get; set; }
        public List<DesgloseXSeccion> tablaSeccion { get; set; }
        public List<Actas> tablaActasSeccion { get; set; }
        public DiputacionSeccion()
        {
            tablaSeccion = new List<DesgloseXSeccion>();
            tablaActasSeccion = new List<Actas>();
        }

    }
}
