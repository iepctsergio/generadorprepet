﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class CandidaturaIndependiente
    {
        public decimal totalVotos { get; set; }       
        public List<VotosCandidatura> tablaVotosCandidato { get; set; }

        public List<ClaseGeneral> tablaDistrito { get; set; }
        public List<Actas> tablaActasPartido { get; set; }
    }


    public class CandidaturaIDistrito
    {
        public decimal totalVotos { get; set; }

        public List<VotosCandidatura> tablaVotosCandidato;
        public List<Actas> tablaActasDistrito { get; set; }
        public List<DistribucionVotos> tablaVotosPartido { get; set; }
        public List<ClaseGeneral> tablaDistrito { get; set; }
    }
}
