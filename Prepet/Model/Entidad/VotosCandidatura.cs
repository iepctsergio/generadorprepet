﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class VotosCandidatura
    {
        public int idPartido { get; set;  }
        public int idCandidato { get; set;  }
        public int TipoCandidatura_ID { get; set; }
        public string nombreCandidato { get; set; }
        public string urlFoto { get; set; }
        public string logoPartido { get; set; }
        public decimal porcentaje { get; set; }
        public decimal totalVotos { get; set; }
        public int orden { get; set; }
        public bool max { get; set; }
        public List<Partidos> partidos { get; set; }
    }

    public class CandidaturaX
    {
        public int TipoCandidatura_ID { get; set; }
        public string Color { get; set; }
        public string nombreCandidato { get; set; }
        public string logoPartido { get; set; }
        public decimal porcentaje { get; set; }
        public decimal totalVotos { get; set; }
        public int orden { get; set; }
        public bool max { get; set; }
        public int total_obtenidos { get; set; }
        public List<VotacionX> Obtenidos { get; set; }
    }

}
