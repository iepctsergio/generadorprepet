﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class Partidos
    {
        public string NombrePartido { get; set; }
        public string Otros { get; set; }
        public int? votos { get; set; }
    }

    public class DesgloseXPartidos
    {
        public int id { get; set; }
        public int id2 { get; set; }
        public string NombrePartido { get; set; }
        public string Otros { get; set; }
        public int votos { get; set; }
        public int orden { get; set; }
    }

 public class VotacionX{
     public int id { get; set; }
     public string  Nombre { get; set; }
     public int votos { get; set; }
}

}
