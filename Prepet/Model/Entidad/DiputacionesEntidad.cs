﻿using Prepet.Model.Diputacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class DiputacionesEntidad
    {
        public InformacionGeneral informacionGral;

        public CandidaturaPartidoDiputacion votosCandidatura;

        public CandidaturaIndependiente votosCandidaturaIndependiente;

        public DiputacionesEntidad() {
            //informacionGral = new InformacionGeneral();
            votosCandidatura = new CandidaturaPartidoDiputacion();
            votosCandidaturaIndependiente = new CandidaturaIndependiente();
        }
        //public InformacionGeneral informacionGral { get; set; }

        //public List<VotosCandidatura> tablaVotosCandidato { get; set; }


    }

    public class CandidaturaPartidoDiputacion
    {
        public decimal totalVotos { get; set; }
        public List<CandidaturaX> tablaVotosCandidato { get; set; }

        public List<ClaseGeneral> tablaDistrito { get; set; }

        public List<Actas> tablaActasDistrito { get; set; }

    }
}
