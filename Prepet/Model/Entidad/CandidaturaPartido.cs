﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class CandidaturaPartido
    {
        public decimal totalVotos { get; set; }
        public List<VotosCandidatura> candidatos;
        public List<VotosCandidatura> tablaVotosCandidato;
        public List<ClaseGeneral> tablaDistrito { get; set; }
        public List<DistribucionVotosXCandidatura> tablaVotosPartido { get; set; }
        public List<Actas> tablaActasDistrito { get; set; }
        public CandidaturaPartido() {
            candidatos = new List<VotosCandidatura>();
            tablaVotosCandidato = new List<VotosCandidatura>();
            tablaVotosPartido = new List<DistribucionVotosXCandidatura>();
        }
    }
}
