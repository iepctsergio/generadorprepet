﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class SeccionMunicipio
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public decimal TotalVotos { get; set; }
        public List<DesgloseXSeccion> tablaSeccion { get; set; }
        public List<Actas> tablaActasSeccion { get; set; }
        public SeccionMunicipio()
        {
            tablaSeccion = new List<DesgloseXSeccion>();
            tablaActasSeccion = new List<Actas>();
        }

    }

}
