﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class InformacionGeneral
    {
        public decimal actasCapturadas { get; set; }
        public decimal actasEsperadas { get; set; }
        public decimal actasRecibidas { get; set; }
        public int contabilizadas { get; set; }
        public int actasCasillaUrbana { get; set; }
        public int actasCasillaNoUrbana { get; set; }
        public decimal actasListaNominal { get; set; }
        public int casillaBasica { get; set; }
        public int casillaContigua { get; set; }
        public int casillaExtraordinaria { get; set; }
        public decimal casillaEspecial { get; set; }
        public decimal TotalActasListaNominal { get; set; }
        public decimal porcentajecapturadas { get; set; }
        public decimal porcentajecontabilizadas { get; set; }
        public decimal porcentajeParticipacionCiudadana { get; set; }

       /* public int pcasillaBasica { get; set; }
        public int pcasillaContigua { get; set; }
        public int pcasillaExtraordinaria { get; set; }
        public decimal pcasillaEspecial { get; set; }
        public decimal ppcxpartidos { get; set; }
        */

        public decimal Participacion { get; set; }
        public string horaCorte { get; set; }
        public string fechaCorte { get; set; }
        public int timer_update { get; set; }
        public bool ultimo_corte { get; set; }
    }
}
