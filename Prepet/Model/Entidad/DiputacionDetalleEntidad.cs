﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class DiputacionDetalleEntidad
    {
        public InformacionGeneral informacionGral;
        public CandidaturaPartidoDiputacion votosCandidatura;
        public CandidaturaIndependiente votosCandidaturaIndependiente;
        


        public DiputacionDetalleEntidad() {
            //informacionGral = new InformacionGeneral();
            votosCandidatura = new Entidad.CandidaturaPartidoDiputacion();
            votosCandidaturaIndependiente = new CandidaturaIndependiente();
    }
    }

    public class AyuntamientoDetalleEntidad
    {        
        public CandidaturaPartidoDiputacion votosCandidatura;
        public CandidaturaIndependiente votosCandidaturaIndependiente;


        public AyuntamientoDetalleEntidad()
        {           
            votosCandidatura = new Entidad.CandidaturaPartidoDiputacion();
            votosCandidaturaIndependiente = new CandidaturaIndependiente();
        }
    }
}
