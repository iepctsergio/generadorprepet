﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class ClaseGeneral
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public decimal? TotalVotos { get; set; }
        public List<Partidos> Partidos { get; set; }
    }

    public class DesgloseXSeccion
    {
        public int id { get; set; }
        public string idSeccion { get; set; }
        public string Nombre { get; set; }
        public decimal TotalVotos { get; set; }
        public List<DesgloseXPartidos> Partidos { get; set; }
    }

    public class Seccion
    {
        public string distrito { get; set; }
        public string id { get; set; }
        public string Nombre { get; set; }
    }
    }
