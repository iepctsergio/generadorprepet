﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class DetalleMuncipios
    {
        public CandidaturaDetalleMunicipio votosCandidatura;
        public AyuntamientoCandidaturaIndependiente votosCandidaturaIndependiente;

        public DetalleMuncipios()
        {
            votosCandidatura = new CandidaturaDetalleMunicipio();
            votosCandidaturaIndependiente = new AyuntamientoCandidaturaIndependiente();
        }
    }

    public class CandidaturaDetalleMunicipio
    {
        public List<DiputacionDistrito> listaDetalleMunicipio { get; set; }

        public CandidaturaDetalleMunicipio()
        {
            listaDetalleMunicipio = new List<DiputacionDistrito>();
        }
    }

    public class AyuntamientoCandidaturaIndependiente
    {
        public List<DiputacionDistrito2> listaDetalleMunicipio { get; set; }

        public AyuntamientoCandidaturaIndependiente()
        {
            listaDetalleMunicipio = new List<DiputacionDistrito2>();
        }
    }

}
