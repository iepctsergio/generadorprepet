﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Model.Entidad
{
    public class AyuntamientoEntidad
    {        

        public CandidaturaPartidoAyuntamiento votosCandidatura;

        public CandidaturaIndependiente votosCandidaturaIndependiente;

        public AyuntamientoEntidad()
        {           
            votosCandidatura = new CandidaturaPartidoAyuntamiento();
            votosCandidaturaIndependiente = new CandidaturaIndependiente();
        }
    }

    public class CandidaturaPartidoAyuntamiento
    {
        public decimal totalVotos { get; set; }
        public List<CandidaturaX> tablaVotosCandidato { get; set; }
    }
}
