﻿using Prepet.Conexion;
using Prepet.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Estructura
{
    public class DataEstructuraGobernatura
    {
        IEnumerable<Dictionary<string, object>> dataTableCandidatura = null;
        IEnumerable<Dictionary<string, object>> dataTablePartidos = null;
        IEnumerable<Dictionary<string, object>> dataPartidos = null;
        
        public DataEstructuraGobernatura() {
            dataTableCandidatura = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XCandidatura_XTipoEleccion('ORD-2018',2)");
            dataTablePartidos = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XPartido_XTipoEleccion('ORD-2018',2)");
            dataPartidos = new ConexionBD().IEnumerableCollectionData("select * from vw_prepet_desglose_candidatura_grupal");
        }

        public object Candidatos() {

            var result = from row in dataTableCandidatura
                         where int.Parse(row["TipoCandidatura_ID"].ToString()) != 4
                         select new
                         {
                             nombreCandidato = row["Siglas"].Equals("PVEM")?"Registro Cancelado":row["NombreCandidato"],
                             logoPartido = row["Siglas"].Equals("PVEM") ?"PVEM_null.png": row["Logo"],
                             Clave = row["Siglas"],
                             urlFoto = row["Persona_ID"] != null ? row["Persona_ID"] + ".png" : "",
                             partidos = int.Parse(row["TipoCandidatura_ID"] + "") == 3 ? ObtenerPartidos(row["Partido_ID"]) : RegresaLogo(row),
                         };

        return result.ToList();
        }
        private object RegresaLogo(Dictionary<string, object> row)
        {
            var lista = new List<object>();
             lista.Add(new { NombrePartido = row["Siglas"].Equals("PVEM")? "Registro Cancelado" : row["Logo"] });

            return lista;
        }
        private object ObtenerPartidos(object id_partido)
        {
            int id = 0;
            if (int.TryParse(id_partido + "", out id))
            {
                var result = from row in dataPartidos
                             where int.Parse(row["CandidaturaGrupal_ID"] + "") == id
                             select new
                             {
                                 NombrePartido = row["Partido_Clave"].Equals("PVEM") ? "Registro Cancelado": row["Logo"]                                 
                             };
                return result.ToList();
            }
            return null;
        }
        public object Candidaturas()
        {

            var result = from row in dataTableCandidatura
                         where int.Parse(row["TipoEleccion_ID"].ToString()) == 2 && row["Siglas"].ToString() != "VT"
                         select new
                         {
                             nombreCandidato = row["Siglas"].ToString().Equals("PVEM") ?"Registro Cancelado": row["Siglas"].ToString().Equals("CNR") ? Util.CNR : row["Siglas"].ToString().Equals("VN") ? Util.VN : row["NombreCandidato"],
                             logoPartido = row["Siglas"].ToString().Equals("PVEM") ?"": Util.LogoEstructura(row["Logo"]),
                             Clave= row["Siglas"]

                         };
            return result.ToList();
        }    
        public object GobPartidos() {
            var result = from row in dataTablePartidos
                         where (int.Parse(row["TipoEleccion_ID"].ToString()) == 2 && row["Siglas"].ToString() != "VT") && int.Parse(row["NumeroAmbito"].ToString()) == 1
                         select new
                         {
                             nombreCandidato = row["Siglas"].ToString().Equals("PVEM") ? "Registro Cancelado" : row["Siglas"].ToString().Equals("CNR") ? Util.CNR : row["Siglas"].ToString().Equals("VN") ? Util.VN : "",
                             Clave = row["Siglas"],
                             logoPartido = row["Siglas"].ToString().Equals("PVEM") ? "" : Util.LogoEstructura(row["Logo"])
                         };
            return result.ToList();
        }
        public object SeccionPartidos()
        {
            var data= new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_nvlSeccion_XDistrito_Gubernatura('ORD-2018')");
            var result = from row in data
                         where (int.Parse(row["TipoEleccion_ID"].ToString()) == 2 && row["Clave"].ToString() != "VT")
                         select new
                         {                             
                             columna = row["Clave"].ToString().Equals("CNR") ? "NO_REGISTRADOS" : 
                                       row["Clave"].ToString().Equals("CIND") ? "CAND_IND" :
                                       row["Clave"].ToString().Equals("VN") ? "NULOS" : row["Clave"],
                             logo = row["Siglas"].ToString().Equals("PVEM") ? "Registro Cancelado" : row["Clave"].ToString().Equals("CNR") ? Util.CNR : row["Clave"].ToString().Equals("VN") ? Util.VN : row["Logo"]
                         };
            return result.ToList();
    }
        public object GobDistribucionVotos()
        {

            var result = from row in dataTableCandidatura
                         where int.Parse(row["TipoCandidatura_ID"].ToString()) != 4
                         select new
                         {
                             candidato = row["Siglas"].Equals("PVEM") ?"Registro Cancelado":row["NombreCandidato"],
                             Clave = row["Siglas"],
                             partido = row["Siglas"].Equals("PVEM")?"": int.Parse(row["TipoCandidatura_ID"] + "") == 3 ? ObtenerPartidos(row["Partido_ID"]) : RegresaLogo(row),
                         };

            return result.ToList();
        }
        public object GobDistritos()
        {

            return new Conexion.ConexionBD().IEnumerableCollectionData("select DistritoElectoral_ID as id,Descripcion as Nombre from vw_prepet_distritos where ProcesoElectoral_ID='ORD-2018'");
        }
        public object GobPartidosDistritos(int ambito)
        {
            var result = from row in dataTablePartidos
                         where int.Parse(row["NumeroAmbito"].ToString()) == ambito
                         select new
                         {
                             Clave = row["Siglas"],
                             logoPartido = row["Siglas"].Equals("PVEM") ? "Registro Cancelado":Util.LogoEstructura(row["Logo"])
                         };
            return result.ToList();
        }
    }

    public class DataEstructuraMunicipiosDiputacion {

        IEnumerable<Dictionary<string, object>> dataTableCandidatura = null;
        IEnumerable<Dictionary<string, object>> dataTablePartidos = null;
        IEnumerable<Dictionary<string, object>> dataMunicipios = null;
        IEnumerable<Dictionary<string, object>> dataPartidos = null;
        IEnumerable<Dictionary<string, object>> dataEstructuraXDistrito = null;
        string Proceso;
        int AmbitoID;

        public DataEstructuraMunicipiosDiputacion(string Proceso, int AmbitoID) {
            this.Proceso = Proceso;
            this.AmbitoID = AmbitoID;
           //dataTableCandidatura = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XCandidatura_XTipoEleccion('"+ Proceso + "',"+AmbitoID+")");
            dataTableCandidatura = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_nvlEntidad_Candidatura_Un_Independiente('" + Proceso + "'," + AmbitoID + ")");
            dataTablePartidos = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XPartido_XTipoEleccion('" + Proceso + "'," + AmbitoID + ")");
            string sql = "";
            if (AmbitoID == 3)
                sql = "select MunicipioElectoral_ID as id,  UPPER(Nombre) Nombre from vw_prepet_municipios";
            else
                sql = "select DistritoElectoral_ID as id,Descripcion as Nombre from vw_prepet_distritos";
            dataMunicipios = new ConexionBD().IEnumerableCollectionData(sql);
           dataPartidos = new ConexionBD().IEnumerableCollectionData("select * from vw_prepet_desglose_candidatura_grupal");
           dataEstructuraXDistrito = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_Detalle_XDistrito_Municipio('"+ Proceso + "', "+ AmbitoID + ")");
            
        }

        public object getPartidosCandidaturas()
        {
            var MunicipioCandidatura = from row in dataTableCandidatura
                                       where  row["Siglas"].ToString() != "VT"                             
                                       select new {
                                           Clave = row["Clave"],
                                           logoPartido = (!row["Clave"].Equals("CIND") && !row["Clave"].Equals("CNR") && !row["Clave"].Equals("VN")) ? row["Logo"]:"",
                                           nombreCandidato = row["Clave"].ToString().Equals("CNR") ? Util.CNR : row["Clave"].ToString().Equals("VN") ? Util.VN : row["Clave"].Equals("CIND")?"CANDIDATURAS INDEPENDIENTES":"",
                                           Orden = row["Orden"]
                                     };
            return MunicipioCandidatura.OrderBy(x=>x.Orden).ToList();
        }

        public object getPartidos() {

            var result = from row in dataTablePartidos
                         where row["Siglas"].ToString() != "VT"
                         group row by new { Siglas = row["Siglas"] } into g
                         select new
                         {
                             nombreCandidato = g.FirstOrDefault()["Siglas"].ToString().Equals("CNR") ? Util.CNR : g.FirstOrDefault()["Siglas"].ToString().Equals("VN") ? Util.VN : g.Key.Siglas.Equals("CIND") ? "CANDIDATURAS INDEPENDIENTES" : "",
                             Clave = g.FirstOrDefault()["Siglas"],
                             logoPartido = g.Key.Siglas.Equals("CIND") ? "" : Util.LogoEstructura(g.FirstOrDefault()["Logo"]),
                             Orden = g.FirstOrDefault()["Orden"]
                         };
            return result.OrderBy(x=>x.Orden).ToList();
        }


        public object getPartidosDiputacion()
        {
            var result = from row in dataTablePartidos
                         where row["Siglas"].ToString() != "VT"
                         //group row by new {  Siglas= row["Siglas"] } into g
                         select new
                         {
                             nombreCandidato = row["Siglas"].ToString().Equals("CNR") ? Util.CNR : row["Siglas"].ToString().Equals("VN") ? Util.VN : row["Siglas"].ToString().Equals("CIND") ? "CANDIDATURAS INDEPENDIENTES" : "",
                             Clave = row["Siglas"],
                             logoPartido = row["Siglas"].ToString().Equals("CIND") ? "" : Util.LogoEstructura(row["Logo"])
                         };
            return result.ToList();
        }

        public object getMunicipios() {
            //var ltMunicipios = new ConexionBD().IEnumerableCollectionData("select MunicipioElectoral_ID as id,  UPPER(Nombre) Nombre from vw_prepet_municipios");
            if (dataMunicipios.Count() > 0)
                return dataMunicipios;
            else
                return new object();
        }
        public object getMunicipiosPartidos(object municipios)
        {
            var data_candidaturas = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XCandidatura_XTipoEleccion('" + Proceso + "'," + AmbitoID + ")");
            var data_partidos = new ConexionBD().IEnumerableCollectionData("call PREPET_Estructura_XPartido_XTipoEleccion('" + Proceso + "'," + AmbitoID + ")");

            IEnumerable<Dictionary<string, object>> x = (IEnumerable<Dictionary<string, object>>)municipios;
            var res = from row in x select new {
                id = row["id"],
                Nombre = row["Nombre"],
                Candidatura = from fila in data_candidaturas
                              where (int.Parse(fila["NumeroAmbito"].ToString()) == int.Parse(row["id"].ToString())) && fila["Siglas"].ToString() != "VT"
                              select new
                              {
                                  logoPartido = fila["Siglas"].ToString().Equals("CNR") ? "" : fila["Siglas"].ToString().Equals("VN") ? "" :  fila["Logo"],
                                  Clave = fila["Siglas"],
                                  nombreCandidato = fila["Siglas"].ToString().Equals("CNR") ? Util.CNR : fila["Siglas"].ToString().Equals("VN") ? Util.VN :  fila["NombreCandidato"],
                                  partido = int.Parse(fila["TipoCandidatura_ID"] + "") == 3 ? ObtenerPartidos(fila["Partido_ID"]) : RegresaLogo(fila)
                              },
                Partidos = from fila in data_partidos
                           where (int.Parse(fila["NumeroAmbito"].ToString()) == int.Parse(row["id"].ToString())) && fila["Siglas"].ToString() != "VT"
                           select new
                           {
                               logoPartido = fila["Siglas"].ToString().Equals("CNR") ? "" : fila["Siglas"].ToString().Equals("VN") ? "" :  fila["Logo"],
                               nombreCandidato = fila["Siglas"].ToString().Equals("CNR") ? Util.CNR : fila["Siglas"].ToString().Equals("VN") ? Util.VN : "",
                             Clave = fila["Siglas"],
                         }
        };
            
            return res;
        }

        private object ObtenerPartidos(object id_partido)
        {
            int id = 0;
            if (int.TryParse(id_partido + "", out id))
            {
                var result = from row in dataPartidos
                             where int.Parse(row["CandidaturaGrupal_ID"] + "") == id
                             select new
                             {
                                 NombrePartido = row["Logo"]
                             };
                return result.ToList();
            }
            return null;
        }

        private object RegresaLogo(Dictionary<string, object> row)
        {
            var lista = new List<object>();
            lista.Add(new { NombrePartido = row["Logo"] });
            return lista;
        }

        public object getMunicipiosXSeccion() {

            var result = from x in dataMunicipios
                          select new
                          {
                              id = x["id"],
                              Partidos = getPartidosDistritos(x["id"])
                          };

            return result;
            
        }

        private object getPartidosDistritos(object v)
        {
            if (dataEstructuraXDistrito.Count() > 0) {

               var  result = from x in dataEstructuraXDistrito
                             where (int.Parse(x["NumeroAmbito"].ToString()) == int.Parse(v.ToString()))
                             select new
                             {
                                 columna= x["Siglas"].ToString().Equals("CNR") ? "NO_REGISTRADOS" :
                                       x["Siglas"].ToString().Equals("CIND") ? "CAND_IND" :
                                       x["Siglas"].ToString().Equals("VN") ? "NULOS" : x["Siglas"],
                                 logo= x["Siglas"].ToString().Equals("CNR") ? Util.CNR : x["Siglas"].ToString().Equals("VN") ? Util.VN : x["Logo"]

                             };

                return result;
            }

            return new object();
        }


        public object getPartidoSeccionDistrito()
        {
            if (dataEstructuraXDistrito.Count() > 0)
            {

                var result = from row in dataEstructuraXDistrito
                             where row["Siglas"].ToString() != "VT"
                             group row by new { Siglas = row["Siglas"] } into x
                             select new
                             {
                                 columna = x.FirstOrDefault()["Siglas"].ToString().Equals("CNR")  ? "NO_REGISTRADOS" :
                                           x.FirstOrDefault()["Siglas"].ToString().Equals("CIND") ? "CAND_IND" :
                                           x.FirstOrDefault()["Siglas"].ToString().Equals("VN")   ? "NULOS" : x.FirstOrDefault()["Siglas"],
                                 logo = x.FirstOrDefault()["Siglas"].ToString().Equals("CNR") ? Util.CNR : x.FirstOrDefault()["Siglas"].ToString().Equals("VN") ? Util.VN : x.FirstOrDefault()["Siglas"].ToString().Equals("CIND") ? "CANDIDATURAS INDEPENDIENTES" : x.FirstOrDefault()["Logo"],
                                 Tipo = int.Parse(x.FirstOrDefault()["TipoCandidatura_ID"]+"") == 1 ? 1 : int.Parse(x.FirstOrDefault()["TipoCandidatura_ID"] + "") == 2? 3: int.Parse(x.FirstOrDefault()["TipoCandidatura_ID"] + "") == 3? 2: int.Parse(x.FirstOrDefault()["TipoCandidatura_ID"] + ""),
                             };

                return result.OrderBy(x=>x.Tipo);
            }

            return new object();
        }
    }

    public class Secciones {
       
        public IEnumerable<Dictionary<string, object>> getSecciones() {
            var seccion = new ConexionBD().IEnumerableCollectionData("SELECT DISTINCT LPAD(SE.SeccionElectoral_Numero,4,0) AS SECCION,SE.DistritoElectoral_ID as DISTRITO,SE.MunicipioElectoral_ID as MUNICIPIO FROM Prep_Eleccion E JOIN Siee_PadronLista_SeccionElectoral SE ON E.SECCIONELECTORAL_ID WHERE E.TIPOELECCION_ID=2 AND E.ELECCION_ACTIVO;");
            return seccion;

        }
    }
}
