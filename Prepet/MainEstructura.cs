﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Prepet.Estructura;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet
{
    public class MainEstructura
    {
        public void GenerarStructura()
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            ///************************************GUBERNATURA
            #region Genera JSON GOBERNATURA
            var str = new DataEstructuraGobernatura();
            var dc= new Dictionary<string, object>();
            dc.Add("candidatos", str.Candidatos());
            dc.Add("tablaVotosCandidato", str.Candidaturas());
            dc.Add("Partidos", str.GobPartidos());
            dc.Add("DistribucionVotos", str.GobDistribucionVotos());
            dc.Add("Distritos", str.GobDistritos());
            dc.Add("SeccionPartidos", str.SeccionPartidos());
            str = null;

            var dic_gob = new Dictionary<string, object>();
            dic_gob.Add("gobEntidad", dc);
            //escribe el objeto en el json
            using (StreamWriter sw = new StreamWriter(@"c:\Json\Estructura\estructuraGOB.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, dic_gob);
                dic_gob = null;
                str = null;
            }
            //Fin de la estructura por gubernatura
            #endregion
            ///****************************************MUNICIPIOS
            #region Genera JSON MUNICIPIOS
            var strM = new DataEstructuraMunicipiosDiputacion("ORD-2018",3);
            var dcM = new Dictionary<string, object>();
            dcM.Add("tablaVotosCandidato", strM.getPartidosCandidaturas());
            dcM.Add("Partidos", strM.getPartidos());
            var municipios = strM.getMunicipios();
            dcM.Add("Municipios", municipios);
            dcM.Add("MunicipiosPartidos", strM.getMunicipiosPartidos(municipios));
            dcM.Add("Secciones", strM.getMunicipiosXSeccion());

            var dic_mun = new Dictionary<string, object>();
            dic_mun.Add("Municipios", dcM);
            //escribe el objeto en el json
            using (StreamWriter sw = new StreamWriter(@"c:\Json\Estructura\estructuraPRES_MUN_REG.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, dic_mun);
                strM = null;
                dcM = null;
            }
            #endregion
            ///****************************************Diputaciones
            #region Genera JSON DIPUTACION
            var strD = new DataEstructuraMunicipiosDiputacion("ORD-2018", 1);
            var dcD = new Dictionary<string, object>();
            dcD.Add("tablaVotosCandidato", strD.getPartidosCandidaturas());
            dcD.Add("Partidos", strD.getPartidos());
            dcD.Add("PartidosSeccionDistritos", strD.getPartidoSeccionDistrito());
            var distritos = strD.getMunicipios();
            dcD.Add("Distritos", distritos);
            dcD.Add("DistritosPartidos", strD.getMunicipiosPartidos(distritos));
            dcD.Add("Secciones", strD.getMunicipiosXSeccion());

            var dic_dip = new Dictionary<string, object>();
            dic_dip.Add("Diputacion", dcD);          

            //escribe el objeto en el json
            using (StreamWriter sw = new StreamWriter(@"c:\Json\Estructura\estructuraDIP.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, dic_dip);
                strD = null;
                dcD = null;
            }

            var secciones = new Dictionary<string, object>();
            secciones.Add("Secciones", new Secciones().getSecciones());
            using (StreamWriter sw = new StreamWriter(@"c:\Json\Estructura\secciones.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, secciones);
                strD = null;
                dcD = null;
            }

            #endregion
        }

    }
}
