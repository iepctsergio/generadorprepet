﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Conexion
{
    public class ConexionBD
    {
       // private DbConnection dbcon;
        //private DbCommand dbcomm;
        private string cadena;
        private static DbProviderFactory fabricante=null;
        string provider = @"MySql.Data.MySqlClient";
        MySqlConnection con = null; 

        public ConexionBD() {
            try
            {

                cadena = @"server = 192.168.200.4; user id = siee_prepet_demo; password = Si33?r3?37; database = BDInstitucional";
                //cadena = @"server = 192.168.11.245; user id = root; password = toor; database = BDInstitucional_desa";
                //cadena = @"server = 192.168.11.250; user id = root; password = iEpcT2017_; database = BDInstitucional_Prep";

            }
            catch (Exception ex) {
                throw new Exception("Error al intentar obtener la cadena de conexión",ex);
            }
        }

        protected void Conectar()
        {
            try
            {
                if (con == null)
                {
                    con = new MySqlConnection(cadena);
                    fabricante = DbProviderFactories.GetFactory(provider);
                    this.con.Open();
                }
            }
            catch (Exception ex) {
                throw new Exception("Ocurrio un error al iniciar la conexion",ex);
            }
        }

        protected void DesConectar()
        {
            try
            {
                this.con.Close();
                this.con = null;
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrio un error al iniciar la conexion", ex);
            }
        }

        public DataTable ejecutarQuery(string query) {

            Conectar();
            DataTable datos = new DataTable();
            try
            {
                MySqlDataAdapter adaptador = new MySqlDataAdapter(query, con);
                //adaptador.SelectCommand.CommandTimeout = 360;//30 segundos
                adaptador.Fill(datos);
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return null;
            }
            finally
            {
                this.DesConectar();
            }
            return datos;
        }
        public IEnumerable<Dictionary<string, object>> IEnumerableCollectionData(string query)
        {
            DataTable table = ejecutarQuery(query);
            if (table == null)
                return null;
            return table
               .AsEnumerable()
               .Select(r => table.Columns.OfType<DataColumn>()
                   .Select(c => new { Column = c.ColumnName, Value = r[c] })
                   .ToDictionary(i => i.Column, i => i.Value != DBNull.Value ? i.Value : null));
        }


        public IEnumerable<Dictionary<string, object>> ConvertIEnumerableCollection(DataTable table)
        {
            if (table == null)
                return null;
            return table
               .AsEnumerable()
               .Select(r => table.Columns.OfType<DataColumn>()
                   .Select(c => new { Column = c.ColumnName, Value = r[c] })
                   .ToDictionary(i => i.Column, i => i.Value != DBNull.Value ? i.Value : null));
        }
        public IEnumerable<Dictionary<string, object>> IEnumerableCollectionCustomColumn(DataTable table) //string query)
        {
           // DataTable table = ejecutarQuery(query);
            if (table == null)
                return null;
            removerColumnas(table);


            return table
               .AsEnumerable()
               .Select(r => table.Columns.OfType<DataColumn>()
                   .Select(c => new { Column = c.ColumnName, Value = r[c] })
                   .ToDictionary(i => i.Column, i => i.Value != DBNull.Value ? i.Value : null));
        }

        private void removerColumnas(DataTable table)
        {
            table.Columns.Remove("ID_ESTADO");
            table.Columns.Remove("ESTADO");
            table.Columns.Remove("TIPO_CASILLA");
            table.Columns.Remove("EXT_CONTIGUA");
            table.Columns.Remove("NUMAMBITO_ID");
            if (table.Columns.Contains("ID_DISTRITO"))
            {
                table.Columns.Remove("ID_DISTRITO");
                table.Columns.Remove("DISTRITO");
            }
            if (table.Columns.Contains("ID_MUNICIPIO"))
            {
                table.Columns.Remove("ID_MUNICIPIO");
                table.Columns.Remove("MUNICIPIO");
            }
            table.Columns.Remove("UBICACION");
            table.Columns.Remove("TOTAL_BOLETAS_SOBRANTES");
            table.Columns.Remove("TOTAL_PERSONAS_VOTARON");
            table.Columns.Remove("TOTAl_REP_PARTIDO_CI_VOTARON");
            table.Columns.Remove("TOTAL_VOTOS_SACADOS");
            table.Columns.Remove("Eleccion_ID");
            table.Columns.Remove("ORIGEN");

            table.Columns.Remove("SHA");
            table.Columns.Remove("FECHA_HORA_ACOPIO");
            table.Columns.Remove("SeguimientoRegistroActa_Cotejo");
            table.Columns.Remove("FECHA_HORA_CAPTURA");
            table.Columns.Remove("FECHA_HORA_VERIFICACION");

            table.Columns.Remove("Observacion_x");
            //table.Columns.Remove("Observacion1");
            table.Columns.Remove("SeguimientoDigitalizacion_Hash_x");
            table.Columns.Remove("HORA_ACOPIO_x");
            table.Columns.Remove("TipoDocumento_Nombre_x");
            table.Columns.Remove("SeguimientoDigitalizacion_Movil");
            table.Columns.Remove("HAYINCONSISTENCIA");
            table.Columns.Remove("CLAVE_CASILLA");
            table.Columns.Remove("CLAVE_ACTA");
            table.Columns.Remove("TOTAL_VOTOS_ASENTADO");
            table.Columns.Remove("REPRESENTANTES_PP_CI");
            table.Columns.Remove("MECANISMOS_TRASLADO");
        }

        public DataTable EjecutaProcedimiento(string Procedimiento)
        {
            Conectar();
            var dt3 = ejecutarQuery(Procedimiento);
            var x = dt3.Rows.Count;
            return dt3; 
        }

        public string ExportCSV(DataTable dt, string csv,int modulo)
        {
            //string csv = string.Empty;
            //DataTable dt = ejecutarQuery(query);
            if (dt == null)
                return null;
            else if (dt.Rows.Count == 0)
                return null;

            dt.Columns.Remove("AMBITO_ID");
            dt.Columns.Remove("AMBITO_NOMBRE");
            
            dt.Columns.Remove("PrincipioEleccion_ID");
            dt.Columns.Remove("UBICACION");
            dt.Columns.Remove("NUMAMBITO_ID");
            dt.Columns.Remove("SeccionElectoral_ID");
            dt.Columns.Remove("Casilla_ID");
            dt.Columns.Remove("Seguimiento");
            dt.Columns.Remove("RegistroActa_Publicacion");
            dt.Columns.Remove("SeguimientoDigitalizacion_ID");
            dt.Columns.Remove("SeguimientoRegistroActa_Cotejo");
            dt.Columns.Remove("ImagenActa");
            //dt.Columns.Remove("VT");
            dt.Columns.Remove("Eleccion_ID");


            dt.Columns.Remove("Casilla_Descripcion");
            dt.Columns.Remove("TIPO_CASILLA_ID");
            dt.Columns.Remove("Observacion_x");
            //dt.Columns.Remove("Observacion1");
            dt.Columns.Remove("SeguimientoDigitalizacion_Hash_x");
            dt.Columns.Remove("HORA_ACOPIO_x");
            dt.Columns.Remove("TipoDocumento_Nombre_x");
            dt.Columns.Remove("SeguimientoDigitalizacion_Movil");
            dt.Columns.Remove("HAYINCONSISTENCIA");
            //dt.Columns.Remove("OBSERVACIONES1");

            IEnumerable<string> columnNames= dt.Columns.Cast<DataColumn>()
                .Select(x => string.Concat("\"", x.ToString().Replace("\"", "\"\""), "\""));
            var rsc = string.Join(",", columnNames);
            csv += rsc;
           
           
            //Add new line.
            csv += "\r\n";

            foreach (DataRow row in dt.Rows)
            {
               IEnumerable<string> fields = row.ItemArray
                    .Where(x => !x.Equals("PrincipioEleccion_ID"))
                    .Select(field =>
                string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                var rsr= string.Join(",", fields);
                csv += rsr;               
                csv += "\r\n";
            }
            return csv;
        }

        public string[] HoraCorte() {
            var result = ejecutarQuery("SELECT DATE_FORMAT(SYSDATE(),'%H:%i') AS HORA, DATE_FORMAT(SYSDATE(),'%d/%m/%Y') AS FECHA");
            String[] array = { "" + result.Rows[0]["HORA"], "" + result.Rows[0]["FECHA"] };
            return array;
        }
    }
}
