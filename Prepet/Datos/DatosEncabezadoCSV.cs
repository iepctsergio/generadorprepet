﻿using Prepet.Conexion;
using Prepet.Model;
using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Datos
{
     public class DatosEncabezadoCSV
    {
        public DataTable dataTableSeccion = null;
        /*public DatosEncabezadoCSV(string pProcesoElectoralID, int pTipoEleccionID, int pNumeroAmbitoID)
        { 
            dataTableSeccion = new ConexionBD().ejecutarQuery("call PREPET_spDesgloseXCasilla('" + pProcesoElectoralID + "', " + pTipoEleccionID + ", " + pNumeroAmbitoID + ")");
        }*/

        public DatosEncabezadoCSV(DataTable table)
        {
            dataTableSeccion = table;
        }
        public InfGeneralCSV InformacionGenerales(string HoraCorte,string Fecha, string Eleccion,bool ultimo_corte)
        {           

            var dataCasilla= new ConexionBD().ConvertIEnumerableCollection(dataTableSeccion);
            if (dataCasilla == null || dataCasilla.Count() == 0)
                return new InfGeneralCSV();
            var infoGeneral = new InfGeneralCSV();
            infoGeneral.TIPO_ELECCION = Eleccion;
            infoGeneral.FECHA_CORTE = Fecha+" "+ HoraCorte+ " (UTC-6)";
            infoGeneral.ACTAS_ESPERADAS = dataCasilla.Count();
            infoGeneral.ACTAS_FUERA_DE_CATALOGO = 0;//Revisar
            infoGeneral.ACTAS_CAPTURADAS = dataCasilla.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
            infoGeneral.ACTAS_REGISTRADAS = infoGeneral.ACTAS_CAPTURADAS;
            infoGeneral.PORCENTAJE_ACTAS_CAPTURADAS = Util.TruncateDecimal(((infoGeneral.ACTAS_CAPTURADAS / infoGeneral.ACTAS_ESPERADAS) * 100),4);

            infoGeneral.ACTAS_CONTABILIZADAS = dataCasilla.Where(x => x["CONTABILIZADA"]!=null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
            
            infoGeneral.PORCENTAJE_ACTAS_CONTABILIZADA =Util.TruncateDecimal(((infoGeneral.ACTAS_CONTABILIZADAS / infoGeneral.ACTAS_ESPERADAS) * 100),4);
            var num_actas_con_inconsistencia = dataCasilla.Where(x => int.Parse(x["HAYINCONSISTENCIA"].ToString()) == 1).Count();

            if(num_actas_con_inconsistencia>0)
            infoGeneral.PORCENTAJE_ACTAS_CON_INCONSISTENCIAS = Util.TruncateDecimal(((num_actas_con_inconsistencia / infoGeneral.ACTAS_ESPERADAS) * 100), 4); ;//Revisar

            infoGeneral.ACTAS_NO_CONTABILIZADAS = dataCasilla.Where(x => (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 0) && int.Parse(x["RegistroActa_Publicacion"].ToString())==1).Count();

            int b = 0, c = 0, ex = 0, e = 0;
            decimal casillaBasica = dataCasilla.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out b) ? b : 0);
            decimal casillaContigua = dataCasilla.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out c) ? c : 0);
            decimal casillaExtraordinaria = dataCasilla.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out ex) ? ex : 0);
            decimal casillaEspecial = dataCasilla.Where(x => (int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 5 || int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 6) && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out e) ? e : 0);

            int actasListaNominal = dataCasilla.Where(x => x["CONTABILIZADA"] != null &&  int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(x => int.Parse(x["LISTA_NOMINAL"] + ""));
            decimal Participacion= casillaBasica + casillaContigua + casillaExtraordinaria + (ultimo_corte ? casillaEspecial : 0); 
            if (actasListaNominal > 0)
            {
                var p = ((Participacion * 100) / actasListaNominal);
                if (p > 100)
                    p = 100;
                infoGeneral.PORCENTAJE_PARTICIPACION_CIUDADANA = Util.TruncateDecimal(p, 4);
            }

            return infoGeneral;
        }
       
    }
    public class InfGeneralCSV {
        public string TIPO_ELECCION { get; set; }
        public string FECHA_CORTE { get; set; }
        public decimal ACTAS_ESPERADAS { get; set; }
        public decimal ACTAS_FUERA_DE_CATALOGO { get; set; }
        public decimal ACTAS_REGISTRADAS { get; set; }
        public decimal ACTAS_CAPTURADAS { get; set; }
        public decimal PORCENTAJE_ACTAS_CAPTURADAS { get; set; }
        public decimal ACTAS_CONTABILIZADAS { get; set; }
        public decimal PORCENTAJE_ACTAS_CONTABILIZADA {get;set;}
        public decimal PORCENTAJE_ACTAS_CON_INCONSISTENCIAS { get; set; }
        public decimal ACTAS_NO_CONTABILIZADAS { get; set; }
        public decimal PORCENTAJE_PARTICIPACION_CIUDADANA { get; set; }

    }
}
