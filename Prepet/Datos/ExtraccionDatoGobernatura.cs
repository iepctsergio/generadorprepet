﻿using Prepet.Conexion;
using Prepet.Model;
using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Datos
{
    public class ExtraccionDatoGobernatura
    {

        IEnumerable<Dictionary<string, object>> dataTable = null;
        IEnumerable<Dictionary<string, object>> dataTableCandidatura = null;
        List<ClaseGeneral> ltDistritos = null;
        public IEnumerable<Dictionary<string, object>> dataTableSeccion = null;
        public ExtraccionDatoGobernatura()//string pProcesoElectoralID, int pTipoEleccionID, int pNumeroAmbitoID,bool ultimo_corte)
        {

            //dataTableCandidatura = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXCandidatura('" + pProcesoElectoralID + "'," + pTipoEleccionID + "," + pNumeroAmbitoID + ",1)");
            //dataTable = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXPartido('" + pProcesoElectoralID + "'," + pTipoEleccionID + "," + pNumeroAmbitoID + ",1)");
            //dataTableSeccion = new ConexionBD().IEnumerableCollectionCustomColumn("call PREPET_spDesgloseXCasilla('" + pProcesoElectoralID + "', " + pTipoEleccionID + ", " + pNumeroAmbitoID + ")");
        }

        public void DataTableSeccionCasilla(DataTable table, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable)
        {
            this.dataTableSeccion = new ConexionBD().IEnumerableCollectionCustomColumn(table);
            this.dataTableCandidatura = dataTableCandidatura;
            this.dataTable = dataTable;
        }
        public InformacionGeneral InformacionGenerales(string HoraCorte,bool ultimo_corte)
        {
            if (dataTableSeccion == null|| dataTableSeccion.Count() == 0)
                return new InformacionGeneral();
            var infoGeneral = new InformacionGeneral();
            infoGeneral.actasEsperadas = dataTableSeccion.Count();
            infoGeneral.actasCapturadas = dataTableSeccion.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
            infoGeneral.contabilizadas = dataTableSeccion.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
            int b = 0, c = 0, ex = 0, e = 0;
            infoGeneral.casillaBasica = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "",out b)? b:0);
            infoGeneral.casillaContigua = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out c) ? c : 0);
            infoGeneral.casillaExtraordinaria = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out ex) ? ex : 0);
            infoGeneral.casillaEspecial = dataTableSeccion.Where(x => (int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 5 || int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 6) && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out e) ? e : 0);
            infoGeneral.actasListaNominal = dataTableSeccion.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(x => int.Parse(x["LISTA_NOMINAL"] + ""));

            infoGeneral.porcentajecapturadas = Util.TruncateDecimal(((infoGeneral.actasCapturadas / infoGeneral.actasEsperadas) * 100), 4);
            infoGeneral.porcentajecontabilizadas = Util.TruncateDecimal(((infoGeneral.contabilizadas / infoGeneral.actasEsperadas) * 100), 4);


            infoGeneral.actasCasillaUrbana = dataTableSeccion.Where(x => int.Parse(x["UBICACION_CASILLA"].ToString()) == 1 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Count();
            infoGeneral.actasCasillaNoUrbana = dataTableSeccion.Where(x => int.Parse(x["UBICACION_CASILLA"].ToString()) == 2 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Count();
            infoGeneral.TotalActasListaNominal = dataTableSeccion.Sum(s => int.Parse(s["LISTA_NOMINAL"].ToString()));


            infoGeneral.Participacion = infoGeneral.casillaBasica + infoGeneral.casillaContigua + infoGeneral.casillaExtraordinaria + (ultimo_corte ? infoGeneral.casillaEspecial : 0);//TotalVotosCiudadano();
            if (infoGeneral.actasListaNominal > 0)
            {
                var p = ((infoGeneral.Participacion * 100) / infoGeneral.actasListaNominal);
                if (p > 100)
                    p = 100;
                infoGeneral.porcentajeParticipacionCiudadana = Util.TruncateDecimal(p, 4);
            }
           



            //infoGeneral.fechaCorte = DateTime.Now.ToString("dd 'de' MMMM 'de' yyyy");
            infoGeneral.horaCorte = HoraCorte;
            infoGeneral.ultimo_corte = ultimo_corte;
            return infoGeneral;
        }
        
        public List<VotosCandidatura> VotosCandidatura()
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<VotosCandidatura>();

            var ltcandidatura = from row in dataTableCandidatura
                                where row["TipoCandidatura_ID"]!=null
                                group row by
   new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"] }
                        into g
                                select
                          new VotosCandidatura
                          {
                              TipoCandidatura_ID = g.Key.TipoCandidatura_ID!=null? int.Parse(g.Key.TipoCandidatura_ID.ToString()):0,
                              totalVotos = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())),
                              logoPartido = ""+g.FirstOrDefault()["IntegracionVotacion_Clave"],//Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]), //NombreLogoEmblema
                              orden = int.Parse("" + g.FirstOrDefault()["CONCEPTO_ORDEN"]),
                              nombreCandidato = Util.ValidaNombre(g.FirstOrDefault()["Nombre_Persona"], g.FirstOrDefault()["NombreLogoEmblema"])
                          };

            var total = ltcandidatura.Sum(s => s.totalVotos);
             
                List<VotosCandidatura> vt = ltcandidatura.ToList();
            if (vt != null && vt.Count > 0)
            {
                var max = ltcandidatura.Where(x => x.TipoCandidatura_ID != 4).Max(s => s.totalVotos);
                foreach (var item in vt)
                {
                    if (max == item.totalVotos)
                        item.max = true;
                    item.porcentaje = Util.TruncateDecimal((item.totalVotos / total) * 100, 4);
                    item.TipoCandidatura_ID = 0;
                }

                var maximo = vt.Count(x => x.max);
                if (maximo > 1)
                {
                    vt.ForEach(x => x.max = false);
                }
            }
            

            return (vt == null || vt.Count == 0) ? new List<VotosCandidatura>() : vt;

        }
        public List<VotosCandidatura> VotosXPartidosYCandidatura(decimal total)
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<VotosCandidatura>();

            var ltcandidatura = from row in dataTableCandidatura
                                where row["TipoCandidatura_ID"]!=null && int.Parse(row["TipoCandidatura_ID"].ToString()) != 4
                                group row by
  new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"] }
                        into g
                                select
                         new VotosCandidatura
                         {
                             totalVotos = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())),
                             logoPartido = g.FirstOrDefault()["IntegracionVotacion_Clave"]+"",//Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]),//NombreLogoEmblema
                             orden = int.Parse("" + g.FirstOrDefault()["CONCEPTO_ORDEN"]),
                             partidos = Util.getVotos(g.ToList()),
                             nombreCandidato = "" + g.FirstOrDefault()["Nombre_Persona"],
                             urlFoto = Util.getIdPersona(g.ToList(), 1),
                             idCandidato = int.Parse(Util.getIdPersona(g.ToList(), 2))
                         };
            var c = ltcandidatura.Count();
            //var total = ltcandidatura.Sum(s => s.totalVotos);
           
            List<VotosCandidatura> vt = ltcandidatura.ToList();
            if (vt != null&&vt.Count>0)
            {
                var max = ltcandidatura.Max(s => s.totalVotos);

                foreach (var item in vt)
                {
                    if (max == item.totalVotos)
                        item.max = true;
                    item.porcentaje = Util.TruncateDecimal((item.totalVotos / total) * 100, 4);
                    item.TipoCandidatura_ID = 0;
                }
                var maximo= vt.Count(x => x.max);
                if (maximo > 1) {
                    vt.ForEach(x => x.max = false);
                }
            }
            
            return (vt == null || vt.Count == 0) ? new List<VotosCandidatura>() : vt;

        }

        public List<DistribucionVotos> DetalleEntidad()
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<DistribucionVotos>();
            var ltIndependiente = from row in dataTable
                                  group row by
new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"], Orden = row["CONCEPTO_ORDEN"] }
                        into g
                                  select
                           new DistribucionVotos
                           {
                              // Nombre = "" + g.FirstOrDefault()["IntegracionVotacion_Nombre"],
                               Logo = g.FirstOrDefault()["IntegracionVotacion_Clave"]+"",//Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                               Partido = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())),
                               Coalicion = g.Sum(f => !(f["VOTOSXCOALICION"] == null) ? decimal.Parse(f["VOTOSXCOALICION"].ToString()) : 0)
                           };

            List<DistribucionVotos> vt = ltIndependiente.Count() > 0 ? ltIndependiente.ToList() : null;
            return (vt == null || vt.Count == 0) ? new List<DistribucionVotos>() : vt;
        }

        public List<DistribucionVotosXCandidatura> DetalleEntidadVotosCandidatura(decimal total_votacion)
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<DistribucionVotosXCandidatura>();
            var ltIndependiente = from row in dataTableCandidatura
                                  where (row["TipoCandidatura_ID"]!=null && int.Parse(row["TipoCandidatura_ID"].ToString())!=4)
                                  group row by
new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"] }
                        into g
                                  select
                           new DistribucionVotosXCandidatura
                           {
                               candidato = ""+g.Key.ClavePartido,//"" + g.FirstOrDefault()["Nombre_Persona"],
                               partido = Util.getVotos(g.ToList()),
                               total = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString()))
                           };

            List<DistribucionVotosXCandidatura> vt = ltIndependiente.Count() > 0 ? ltIndependiente.ToList() : null;

            //var total = ltIndependiente.Sum(s => s.total);
            if (vt != null)
            {
                if (total_votacion > 0)
                {
                    foreach (var item in vt)
                    {
                        item.porcentaje = Util.TruncateDecimal((item.total / total_votacion) * 100, 4);
                    }
                }
            }
            //NOTA:Revisar el calculo porcentual
            return (vt == null || vt.Count == 0) ? new List<DistribucionVotosXCandidatura>() : vt;
        }

        public List<VotosCandidatura> VotosPartidoCandidaturaIndependiente()
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<VotosCandidatura>();

            var ltcandidaturaIndependiente = from row in dataTable
                                             group row by
  new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"], Orden = row["CONCEPTO_ORDEN"] }
                        into g
                                             select
                                      new VotosCandidatura
                                      {
                                          TipoCandidatura_ID = g.Key.TipoCandidatura_ID != null ? int.Parse(g.Key.TipoCandidatura_ID.ToString()) : 0,
                                          //idPartido = int.Parse(g.Key.PartidoCandidatura_ID.ToString()),
                                          totalVotos = (g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())) 
                                                        + g.Where(vct => vct["VOTOSXCOALICION"] != null).Sum(vc => int.Parse("" + vc["VOTOSXCOALICION"]))),
                                          logoPartido = g.FirstOrDefault()["IntegracionVotacion_Clave"]+"",//Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                                          orden = int.Parse(g.Key.Orden.ToString()),
                                          nombreCandidato = Util.ValidaNombre(g.FirstOrDefault()["Nombre_Persona"], g.FirstOrDefault()["NombreLogoEmblema"])
                                      };

            var total = ltcandidaturaIndependiente.Sum(s => s.totalVotos);
            var max = ltcandidaturaIndependiente.Where(x => x.TipoCandidatura_ID != 4).Max(s => s.totalVotos);
            List<VotosCandidatura> vt = ltcandidaturaIndependiente.ToList();
            if (vt != null)
            {
                foreach (var item in vt)
                {
                    if (max == item.totalVotos)
                        item.max = true;
                    item.porcentaje = Util.TruncateDecimal((item.totalVotos / total) * 100, 4);
                    item.TipoCandidatura_ID = 0;
                }

                var maximo = vt.Count(x => x.max);
                if (maximo > 1)
                {
                    vt.ForEach(x => x.max = false);
                }

            }

            return (vt == null || vt.Count == 0) ? new List<VotosCandidatura>() : vt;

        }

        public List<ClaseGeneral> DistritosVPCandidaturaIndependienteTabla()
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<ClaseGeneral>();
            ltDistritos = (from row in dataTable
                           group row by
                             row["NUMAMBITO_ID"]
                       into g
                           select
                    new ClaseGeneral //NOTA: VERIFICAR ESTA CONSULTA
                    {
                        id = int.Parse(g.FirstOrDefault()["NUMAMBITO_ID"].ToString()),
                        Nombre = "" + g.FirstOrDefault()["NUMAMBITO_NOMBRE"]
                    }).ToList();
            List<ClaseGeneral> vtd = ltDistritos.ToList();
            foreach (var item in vtd)
            {
                item.Partidos = (from row in dataTable
                                 where (int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id)
                                 select new Partidos
                                 {
                                     NombrePartido = row["IntegracionVotacion_Siglas"].ToString().Equals("CNR") ? "CANDIDATOS NO REGISTRADOS" : row["IntegracionVotacion_Siglas"].ToString().Equals("VN") ? "VOTOS NULOS" : Util.ValidaLogo(row["IntegracionVotacion_Siglas"].ToString()),
                                     votos = row["VOTOS"]!=null?int.Parse(row["VOTOS"].ToString())+ (row["VOTOSXCOALICION"]!=null?int.Parse("" + row["VOTOSXCOALICION"]):0) : 0
                                 }).ToList();
                item.TotalVotos = item.Partidos.Sum(x => x.votos.Value);
            }
            return vtd == null ? new List<ClaseGeneral>() : vtd;
        }

        public List<ClaseGeneral> DistritosGobenaturaVotosCandidatura()
        {
            if (dataTableCandidatura == null || dataTable.Count() == 0)
                return new List<ClaseGeneral>();
            ltDistritos = (from row in dataTableCandidatura
                           group row by
                             row["NUMAMBITO_ID"]
                       into g
                           select
                    new ClaseGeneral
                    {
                        id = int.Parse(g.FirstOrDefault()["NUMAMBITO_ID"].ToString()),
                        TotalVotos = g.Where(sum=>sum["VOTOS"]!=null).Sum(s =>int.Parse(s["VOTOS"].ToString()))
                    }).ToList();
            List<ClaseGeneral> vtd = ltDistritos.ToList();
            int countPartidos = 0;
            bool primera_carga = false;
            foreach (var item in vtd)
            {
                item.Partidos = (from row in dataTableCandidatura
                                 where (int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id)
                                 group row by
                                             row["IntegracionVotacion_Clave"]
                                     into g
                                 select new Partidos
                                 {
                                     NombrePartido = g.FirstOrDefault()["IntegracionVotacion_Clave"].ToString().Equals("CNR") ? "CANDIDATOS NO REGISTRADOS" : g.FirstOrDefault()["IntegracionVotacion_Clave"].ToString().Equals("VN") ? "VOTOS NULOS" : Util.ValidaLogo("" +g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                                     votos = g.Where(x=>x["VOTOS"] != null).Sum(s=> int.Parse(s["VOTOS"].ToString()))
                                 }).ToList();
                if (!primera_carga)
                {
                    countPartidos = item.Partidos.Count;
                    primera_carga = true;
                }
                else if (primera_carga)
                {
                    if (item.Partidos.Count < countPartidos)
                    {
                        item.Partidos = new List<Partidos>();
                        for (int i = 0; i < countPartidos; i++)
                        {
                            item.Partidos.Add(new Partidos());
                            item.TotalVotos = null;
                        }
                        countPartidos = item.Partidos.Count;
                    }
                }
               
            }
            return vtd == null ? new List<ClaseGeneral>() : vtd;
        }

       

        public List<Seccion> Secciones()
        {
            if (dataTableSeccion == null || dataTableSeccion.Count() == 0)
                return new List<Seccion>();
            var ltsecciones = from row in dataTableSeccion
                              where int.Parse(""+row["PrincipioEleccion_ID"])==1
                              group row by
                              row["SECCION"]
                        into g
                              select
                       new Seccion
                       {
                           distrito = g.FirstOrDefault()["AMBITO_ID"].ToString(),
                           id= ""+g.FirstOrDefault()["SECCION"],
                           Nombre = "Sección " + g.FirstOrDefault()["SECCION"],
                       };
            return ltsecciones.ToList();
        }

        internal List<Actas> ActasXDistrito(List<ClaseGeneral> lDistritos,bool ultimo_corte)
        {
            if (ltDistritos == null || ltDistritos.Count() == 0)
                return new List<Actas>();

            var actas = new List<Actas>();
            decimal sumaTln = 0, total_votos = 0;
            foreach (var item in lDistritos)
            {
                var casilla = dataTableSeccion.Where(x => int.Parse(x["AMBITO_ID"].ToString()) == item.id);
                var ac = new Actas();
                ac.id = item.id;
                ac.Nombre = item.Nombre;
                ac.Esperadas = casilla.Count();
                ac.Capturadas = casilla.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
                ac.Contabilizadas = casilla.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
                decimal TotalListaNominal = 0;
                if (ultimo_corte)
                {
                    total_votos = item.TotalVotos.Value;
                    sumaTln += item.TotalVotos.Value;
                    TotalListaNominal = casilla.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(s => decimal.Parse(s["LISTA_NOMINAL"].ToString()));

                }
                else
                {
                    total_votos = 0;
                    total_votos = casilla.Where(x => (x["TOTAL_VOTOS_CALCULADO"] != null && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)) &&
                                                (
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                                ))
                                                .Sum(x => x["TOTAL_VOTOS_CALCULADO"] == null ? 0 : int.Parse(x["TOTAL_VOTOS_CALCULADO"].ToString()));
                    sumaTln += total_votos;
                    TotalListaNominal = casilla.Where(x => (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1) &&
                                                (
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                                )
                                                )
                                          .Sum(s => decimal.Parse(s["LISTA_NOMINAL"].ToString()));

                }
                if (TotalListaNominal > 0)
                    ac.Participacion_Ciudadana = TotalListaNominal == 0 ? 0 : Util.TruncateDecimal(((total_votos / TotalListaNominal) * 100),4);
                //Suma de CorteElectoralSeccion_Lista los votos, de las actas que esten ACTACONTABILIZADA = 1
                ac.TotalListaNominal = TotalListaNominal;

                actas.Add(ac);
            }

            var total = new Actas();
            total.id = -1;
            total.Nombre = "Total";
            total.Esperadas = actas.Sum(ae => ae.Esperadas);
            total.Capturadas = actas.Sum(acap => acap.Capturadas);
            total.Contabilizadas = actas.Sum(acon => acon.Contabilizadas);
            total.TotalListaNominal = actas.Sum(tln => tln.TotalListaNominal);
            //var totalVotacion = lDistritos.Where(x=>x.TotalVotos.HasValue).Sum(t => t.TotalVotos.Value);
            if (sumaTln > 0)
                total.Participacion_Ciudadana = Util.TruncateDecimal(((sumaTln / total.TotalListaNominal) * 100), 4);

            actas.Add(total);
            var porcentaje = new Actas();
            porcentaje.id = 0;
            porcentaje.Nombre = "Porcentaje";
            porcentaje.Esperadas = 100;
            porcentaje.Capturadas = Util.TruncateDecimal((total.Capturadas / total.Esperadas) * 100, 4);
            porcentaje.Contabilizadas = Util.TruncateDecimal((total.Contabilizadas / total.Esperadas) * 100, 4);          
            actas.Add(porcentaje);
            return actas.OrderBy(x => x.id).ToList();
        }

        internal List<Actas> ActasDetalleXDistrito(decimal totalVotos,bool ultimo_corte)
        {
            DistritosVPCandidaturaIndependienteTabla();

            if (ltDistritos == null)
                return new List<Actas>();           

            //decimal TotalVotos2 = ltDistritos.Where(x=>x.TotalVotos.HasValue).Sum(x => x.TotalVotos.Value);
            var ActasGeneral = new List<Actas>();
            var total = new Actas();
            total.id = -1;
            total.Nombre = "Total";
            total.Esperadas = dataTableSeccion.Count();
            total.Capturadas = dataTableSeccion.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
            total.Contabilizadas = dataTableSeccion.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
            //Nota: Calcular la partipacion ciudadana
            decimal division = 0;
            if (ultimo_corte)
            {
                total.TotalListaNominal = dataTableSeccion.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(s => int.Parse(s["LISTA_NOMINAL"].ToString()));
                if (totalVotos != 0 && total.TotalListaNominal != 0)
                    division = (totalVotos / total.TotalListaNominal);
                total.Participacion_Ciudadana = division > 0 ? Util.TruncateDecimal(division * 100, 4) : 0;
            }
            else {
                totalVotos = 0;
                totalVotos = dataTableSeccion.Where(x => (x["TOTAL_VOTOS_CALCULADO"] != null && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)) &&
                                               (
                                               int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                               int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                               int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                               ))
                                               .Sum(x => x["TOTAL_VOTOS_CALCULADO"] == null ? 0 : int.Parse(x["TOTAL_VOTOS_CALCULADO"].ToString()));

                total.TotalListaNominal = dataTableSeccion.Where(x => (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1) &&
                                            (
                                            int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                            int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                            int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                            )
                                            )
                                      .Sum(s => decimal.Parse(s["LISTA_NOMINAL"].ToString()));
                if (totalVotos != 0 && total.TotalListaNominal != 0)
                    division = (totalVotos / total.TotalListaNominal);
                total.Participacion_Ciudadana = division > 0 ? Util.TruncateDecimal((division * 100), 4) : 0;
            }

            ActasGeneral.Add(total);

            var porcentaje = new Actas();
            porcentaje.id = 0;
            porcentaje.Nombre = "Porcentaje";
            porcentaje.Esperadas = 100;
            if(total.Capturadas!=0 && total.Esperadas!=0)
             porcentaje.Capturadas = Util.TruncateDecimal((total.Capturadas / total.Esperadas) * 100, 4);
            porcentaje.Contabilizadas = Util.TruncateDecimal((total.Contabilizadas / total.Esperadas) * 100, 4);
            
            ActasGeneral.Add(porcentaje);
            return ActasGeneral.OrderBy(x => x.id).ToList();
        }

        internal List<ClaseGeneral> SeccionListaDistrito()
        {
            if (dataTableSeccion == null || dataTableSeccion.Count() == 0)
                return new List<ClaseGeneral>();
            var ltDistritos = from row in dataTableSeccion
                              group row by
                              row["AMBITO_ID"]
                        into g
                              select
                       new ClaseGeneral
                       {
                           id = int.Parse(g.FirstOrDefault()["AMBITO_ID"].ToString()),
                           Nombre = "" + g.FirstOrDefault()["AMBITO_NOMBRE"],
                       };
            return ltDistritos.ToList();

        }
        public object Actas()
        {
            var resultado = from row in dataTableSeccion
                                //where int.Parse(row["TipoCasilla_ID"].ToString()) != 4
                            select new
                            {
                                Municipio = row["AMBITO_ID"],
                                Seccion = row["SECCION"] + "",//int.Parse(row["SECCION"] + "").ToString("D4"),
                                Casilla_Descripcion = row["Casilla_Descripcion"],
                                Capturadas = row["RegistroActa_Publicacion"],
                                Contabilizada = row["CONTABILIZADA"],
                                ListaNominal = (row["CONTABILIZADA"] != null && int.Parse(row["CONTABILIZADA"] + "") == 1)?row["LISTA_NOMINAL"]:0
                            };
            return resultado.ToList();
        }
    }
}
