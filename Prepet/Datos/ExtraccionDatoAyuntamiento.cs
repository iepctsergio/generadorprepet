﻿using Prepet.Conexion;
using Prepet.Model;
using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Datos
{
    public class ExtraccionDatoAyuntamiento
    {
        IEnumerable<Dictionary<string, object>> dataTable = null;
        IEnumerable<Dictionary<string, object>> dataTableCandidatura = null;
        public List<ClaseGeneral> ltDistritos = null;
        public IEnumerable<Dictionary<string, object>> dataTableSeccion = null;
        public ExtraccionDatoAyuntamiento()// string pProcesoElectoralID, int pTipoEleccionID, int pNumeroAmbitoID,bool ultimo_corte)
        {
            //dataTableCandidatura = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXCandidatura('" + pProcesoElectoralID + "'," + pTipoEleccionID + "," + pNumeroAmbitoID + ",1)");
            //dataTable = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXPartido('" + pProcesoElectoralID + "'," + pTipoEleccionID + "," + pNumeroAmbitoID + ",1)");
            //dataTableSeccion = new ConexionBD().IEnumerableCollectionCustomColumn("call PREPET_spDesgloseXCasilla('" + pProcesoElectoralID + "', " + pTipoEleccionID + ", " + pNumeroAmbitoID + ")");
        }

        public void DataTableSeccionCasilla(DataTable table,IEnumerable<Dictionary<string,object>> dataTableCandidatura, IEnumerable<Dictionary<string,object>> dataTable) {
            this.dataTableSeccion = new ConexionBD().IEnumerableCollectionCustomColumn(table);
            this.dataTableCandidatura = dataTableCandidatura;
            this.dataTable = dataTable;
        }

        public InformacionGeneral InformacionGenerales(string HoraCorte, bool ultimo_corte)
        {
            if (dataTableSeccion == null || dataTableSeccion.Count() == 0)
                return new InformacionGeneral();
            var infoGeneral = new InformacionGeneral();
            infoGeneral.actasEsperadas = dataTableSeccion.Count();
            infoGeneral.actasCapturadas = dataTableSeccion.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
            infoGeneral.contabilizadas = dataTableSeccion.Where(x => x["CONTABILIZADA"]!=null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
            int b = 0, c = 0, ex = 0, e = 0;
            infoGeneral.casillaBasica = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out b) ? b : 0);
            infoGeneral.casillaContigua = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out c) ? c : 0);
            infoGeneral.casillaExtraordinaria = dataTableSeccion.Where(x => int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out ex) ? ex : 0);
            infoGeneral.casillaEspecial = dataTableSeccion.Where(x => (int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 5 || int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 6) && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.TryParse(s["TOTAL_VOTOS_CALCULADO"] + "", out e) ? e : 0);
            infoGeneral.actasListaNominal = dataTableSeccion.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(x => int.Parse(x["LISTA_NOMINAL"] + ""));

            infoGeneral.porcentajecapturadas = Util.TruncateDecimal(((infoGeneral.actasCapturadas / infoGeneral.actasEsperadas) * 100), 4);
            infoGeneral.porcentajecontabilizadas = Util.TruncateDecimal(((infoGeneral.contabilizadas / infoGeneral.actasEsperadas) * 100), 4);


            infoGeneral.actasCasillaUrbana = dataTableSeccion.Where(x => int.Parse(x["UBICACION_CASILLA"].ToString()) == 1 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Count();
            infoGeneral.actasCasillaNoUrbana = dataTableSeccion.Where(x => int.Parse(x["UBICACION_CASILLA"].ToString()) == 2 && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Count();
            infoGeneral.TotalActasListaNominal = dataTableSeccion.Sum(s => int.Parse(s["LISTA_NOMINAL"].ToString()));


            infoGeneral.Participacion = infoGeneral.casillaBasica + infoGeneral.casillaContigua + infoGeneral.casillaExtraordinaria+(ultimo_corte? infoGeneral.casillaEspecial:0);//TotalVotosCiudadano();
            if (infoGeneral.actasListaNominal > 0)
            {
                var p = ((infoGeneral.Participacion * 100) / infoGeneral.actasListaNominal);
                if (p > 100)
                    p = 100;
                infoGeneral.porcentajeParticipacionCiudadana = Util.TruncateDecimal(p, 4);
            }




            //infoGeneral.fechaCorte = DateTime.Now.ToString("dd 'de' MMMM 'de' yyyy");
            infoGeneral.horaCorte = HoraCorte;
            infoGeneral.ultimo_corte = ultimo_corte;
            return infoGeneral;
        }
        


        public List<CandidaturaX> VotosAyuntamientoDistrito()
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<CandidaturaX>();

            int count = 0;
            var NuevoLTcandidatura = from row in dataTableCandidatura
                                     where row["TipoCandidatura_ID"] != null
                                     group row by
       new { TipoCandidatura_ID = row["TipoCandidatura_ID"], Clave_Partido = row["IntegracionVotacion_Clave"] }
                        into g
                                     select
                              new CandidaturaX
                              {
                                  TipoCandidatura_ID = g.Key.TipoCandidatura_ID != null ? int.Parse(g.Key.TipoCandidatura_ID.ToString()) : 0,
                                  Color = "" + g.FirstOrDefault()["COLOR_HEX"],
                                  total_obtenidos = g.Where(lc => lc["LUGARCANDIDATURA"] != null && int.Parse("" + lc["LUGARCANDIDATURA"]) == 1).Count(),
                                  totalVotos = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())),
                                  logoPartido = "" + g.FirstOrDefault()["IntegracionVotacion_Clave"],
                                  Obtenidos = Util.VotosXDistritos(g.ToList()),
                                  orden = count++,
                              };

            List<CandidaturaX> vt = NuevoLTcandidatura.ToList();
            var total = vt.Sum(x => x.totalVotos);
            var max = vt.Count>0? vt.Where(x => x.TipoCandidatura_ID != 4).Max(maxv => maxv.totalVotos):0;
            if (vt != null)
            {
                foreach (var item in vt)
                {
                    if (max == item.totalVotos)
                        item.max = true;
                    item.porcentaje = Util.TruncateDecimal((item.totalVotos / total) * 100, 4);
                    item.TipoCandidatura_ID = 0;
                }
                var maximo = vt.Count(x => x.max);
                if (maximo > 1)
                {
                    vt.ForEach(x => x.max = false);
                }
            }
            return (vt == null || vt.Count == 0) ? new List<CandidaturaX>() : vt;

        }

        public List<VotosCandidatura> VotosPartidoCandidaturaIndependiente()
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<VotosCandidatura>();

            var ltcandidaturaIndependiente = from row in dataTable
                                             where row["TipoCandidatura_ID"]!=null
                                             group row by
  new { TipoCandidatura_ID = row["TipoCandidatura_ID"], Clave_Partido = row["IntegracionVotacion_Clave"] }
                        into g
                                             select
                                      new VotosCandidatura
                                      {
                                          TipoCandidatura_ID = int.Parse(g.Key.TipoCandidatura_ID.ToString()),
                                          //idPartido = int.Parse(g.Key.PartidoCandidatura_ID.ToString()),
                                          totalVotos = (g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())) + g.Where(vct => vct["VOTOSXCOALICION"] != null).Sum(vc => int.Parse("" + vc["VOTOSXCOALICION"]))),
                                          logoPartido =""+g.FirstOrDefault()["IntegracionVotacion_Clave"], //Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                                          //orden = int.Parse(g.FirstOrDefault()["CONCEPTO_ORDEN"].ToString()),
                                          //nombreCandidato = Util.ValidaNombre("", g.FirstOrDefault()["IntegracionVotacion_Clave"])
                                      };

            var total = ltcandidaturaIndependiente.Sum(s => s.totalVotos);
            var max = ltcandidaturaIndependiente.Where(x => x.TipoCandidatura_ID != 4).Max(s => s.totalVotos);
            List<VotosCandidatura> vt = ltcandidaturaIndependiente.ToList();
            if (vt != null)
            {
                foreach (var item in vt)
                {
                    if (max == item.totalVotos)
                        item.max = true;
                    item.porcentaje = Util.TruncateDecimal((item.totalVotos / total) * 100, 4);
                    item.TipoCandidatura_ID = 0;
                }

                var maximo = vt.Count(x => x.max);
                if (maximo > 1)
                {
                    vt.ForEach(x => x.max = false);
                }
            }

            return (vt == null || vt.Count == 0) ? new List<VotosCandidatura>() : vt;

        }

        public List<ClaseGeneral> DistritosAyuntamientoCandidatura()
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<ClaseGeneral>();
            ltDistritos = (from row in dataTableCandidatura
                           group row by
                             row["NUMAMBITO_ID"]
                       into g
                           select
                    new ClaseGeneral
                    {
                        id = int.Parse(g.FirstOrDefault()["NUMAMBITO_ID"].ToString()),
                        Nombre = "" + g.FirstOrDefault()["NUMAMBITO_NOMBRE"]
                    }).ToList();

            var partidosOcoalicion = from row in dataTableCandidatura
                                     where row["TipoCandidatura_ID"] != null
                                     group row by
       new { TipoCandidatura_ID = row["TipoCandidatura_ID"], Clave_Partido = row["IntegracionVotacion_Clave"] }
                       into g
                                     select
                              new Partidos
                              {
                                  Otros = "" + g.Key.Clave_Partido,
                                  NombrePartido = Util.ValidaLogo(g.FirstOrDefault()["NombreLogoEmblema"]),
                              };

            var distritos = ltDistritos.ToList();
            foreach (var item in ltDistritos)
            {
                item.Partidos = new List<Partidos>();
                item.Partidos.AddRange(partidosOcoalicion.ToList());
                foreach (var partido in item.Partidos)
                {
                   
                    partido.votos = dataTableCandidatura.Where(row =>
                         (row["IntegracionVotacion_Clave"] != null && (int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id &&
                         row["IntegracionVotacion_Clave"].ToString().Equals(partido.Otros)))).Aggregate(new int?(), (sum, t) => sum == null ? int.Parse(t["VOTOS"].ToString()) : sum + int.Parse(t["VOTOS"].ToString()));
                }
                item.TotalVotos = item.Partidos.Sum(x => x.votos ?? 0);
            }

            return distritos == null ? new List<ClaseGeneral>() : distritos;
        }

        public List<ClaseGeneral> DistritosAyuntamientoPartidoYcandidaturaIndependiente()
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<ClaseGeneral>();
            var ltDistritosAyuntamiento = (from row in dataTable
                                           group row by
                                             row["NUMAMBITO_ID"]
                        into g
                                           select
                                    new ClaseGeneral
                                    {
                                        id = int.Parse(g.FirstOrDefault()["NUMAMBITO_ID"].ToString()),
                                        Nombre = "" + g.FirstOrDefault()["NUMAMBITO_NOMBRE"]
                                    }).ToList();

            var partidosOcoalicion = from row in dataTable
                                     group row by
       new { TipoCandidatura_ID = row["TipoCandidatura_ID"], Clave_Partido = row["IntegracionVotacion_Clave"] }
                       into g
                                     select
                              new Partidos
                              {
                                  Otros = "" + g.Key.Clave_Partido,
                                  NombrePartido = Util.ValidaLogo(g.FirstOrDefault()["NombreLogoEmblema"]),
                              };

            var distritos = ltDistritosAyuntamiento.ToList();
            foreach (var item in ltDistritosAyuntamiento)
            {
                item.Partidos = new List<Partidos>();
                item.Partidos.AddRange(partidosOcoalicion.ToList());
                foreach (var partido in item.Partidos)
                {
                    //partido.votos =dataTable.
                    //     Where(row => int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id && 
                    //          row["IntegracionVotacion_Clave"].ToString().Equals(partido.Otros))
                    //     .Sum(s => int.Parse(s["VOTOS"].ToString()) + (s["VOTOSXCOALICION"] != null ? int.Parse("" + s["VOTOSXCOALICION"]) : 0));

                    partido.votos = dataTable.Where(row =>
                          int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id &&
                              row["IntegracionVotacion_Clave"].ToString().Equals(partido.Otros)).
                              Aggregate(new int?(), (sum, t) => 
                              sum == null ? int.Parse(t["VOTOS"].ToString()) + (t["VOTOSXCOALICION"] != null ? int.Parse("" + t["VOTOSXCOALICION"]) : 0) :
                              0//sum + (int.Parse(t["VOTOS"].ToString() + (t["VOTOSXCOALICION"] != null ? int.Parse("" + t["VOTOSXCOALICION"]) : 0)))
                              );

                }
                item.TotalVotos = item.Partidos.Sum(x => x.votos ?? 0);
            }

            return distritos == null ? new List<ClaseGeneral>() : distritos;
        }
        internal List<Actas> ActasXDistrito(List<ClaseGeneral> lDistritos,bool ultimo_corte)
        {
            if (ltDistritos == null || ltDistritos.Count() == 0)
                return new List<Actas>();

            var actas = new List<Actas>();
            decimal sumaTln = 0, total_votos = 0; 
            foreach (var item in lDistritos)
            {
                var casilla = dataTableSeccion.Where(x => int.Parse(x["AMBITO_ID"].ToString()) == item.id);
                var ac = new Actas();
                ac.id = item.id;
                ac.Nombre = item.Nombre;
                ac.Esperadas = casilla.Count();
                ac.Capturadas = casilla.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
                ac.Contabilizadas = casilla.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
                decimal TotalListaNominal = 0;
                if (ultimo_corte)
                {
                    total_votos = item.TotalVotos.Value;
                    sumaTln += item.TotalVotos.Value;
                    TotalListaNominal = casilla.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Sum(s => int.Parse(s["LISTA_NOMINAL"].ToString()));
                   // ac.Participacion_Ciudadana = TotalListaNominal == 0 ? 0 : ((item.TotalVotos.Value / TotalListaNominal) * 100);
                   // ac.TotalListaNominal = TotalListaNominal;
                }
                else {
                    total_votos = 0;
                    total_votos = casilla.Where(x => (x["TOTAL_VOTOS_CALCULADO"] != null && (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)) &&
                                                (
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                                ))
                                                .Sum(x => x["TOTAL_VOTOS_CALCULADO"] == null ? 0 : int.Parse(x["TOTAL_VOTOS_CALCULADO"].ToString()));
                    sumaTln += total_votos;
                    TotalListaNominal = casilla.Where(x => (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)  &&
                                                (
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                                )
                                                )
                                          .Sum(s => decimal.Parse(s["LISTA_NOMINAL"].ToString()));

                }
                if (TotalListaNominal > 0)
                    ac.Participacion_Ciudadana = TotalListaNominal == 0 ? 0 : Util.TruncateDecimal(((total_votos / TotalListaNominal) * 100),4);
                //Suma de CorteElectoralSeccion_Lista los votos, de las actas que esten ACTACONTABILIZADA = 1
                ac.TotalListaNominal = TotalListaNominal;

                actas.Add(ac);
            }

            var total = new Actas();
            total.id = -1;
            total.Nombre = "Total";
            total.Esperadas = actas.Sum(ae => ae.Esperadas);
            total.Capturadas = actas.Sum(acap => acap.Capturadas);
            total.Contabilizadas = actas.Sum(acon => acon.Contabilizadas);
            total.TotalListaNominal = actas.Sum(tln => tln.TotalListaNominal);
            //var totalVotacion = lDistritos.Where(t => t.TotalVotos.HasValue).Sum(t => t.TotalVotos.Value);
            if(sumaTln>0)
            total.Participacion_Ciudadana = Util.TruncateDecimal(((sumaTln / total.TotalListaNominal) * 100), 4);

            actas.Add(total);
            var porcentaje = new Actas();
            porcentaje.id = 0;
            porcentaje.Nombre = "Porcentaje";           
            porcentaje.Capturadas = Util.TruncateDecimal((total.Capturadas / total.Esperadas) * 100, 4);
            porcentaje.Contabilizadas = Util.TruncateDecimal((total.Contabilizadas / total.Esperadas) * 100, 4);
            porcentaje.Esperadas = total.TotalListaNominal>0?100:0;           
            actas.Add(porcentaje);
            return actas.OrderBy(x => x.id).ToList();
        }

      

        public List<DiputacionDistrito> getVotosPorPatidos(bool ultimo_corte)
        {
            if (dataTableCandidatura == null || dataTableCandidatura.Count() == 0)
                return new List<DiputacionDistrito>();

            var lista = new List<DiputacionDistrito>();

            foreach (var item in ltDistritos)
            {
                var distrito = new DiputacionDistrito() { id = item.id, Nombre = item.Nombre, TotalVotos = item.TotalVotos.Value };

                var ltPartidos = from row in dataTableCandidatura
                                 where int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id// && (row["TipoCandidatura_ID"]!=null && int.Parse(row["TipoCandidatura_ID"].ToString()) != 4)
                                 group row by
new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"] }
                       into g
                                 select
                          new DistribucionVotosXCandidatura
                          {
                              logoPartido = g.FirstOrDefault()["NombreLogoEmblema"].Equals("CIND") ? (g.FirstOrDefault()["NombreLogoEmblema"].ToString() + g.FirstOrDefault()["Persona_ID"] + ".png") : Util.ValidaLogo(g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                              candidato = Util.ValidaNombre(g.FirstOrDefault()["Nombre_Persona"], g.FirstOrDefault()["IntegracionVotacion_Clave"]),
                              partido = Util.getVotos(g.ToList()),
                              total = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString()))
                          };

                List<DistribucionVotosXCandidatura> vt = ltPartidos.Count() > 0 ? ltPartidos.ToList() : null;

                var total = ltPartidos.Sum(s => s.total);
                if (vt != null)
                {
                    foreach (var partido in vt)
                    {
                        if(total>0)
                        partido.porcentaje = Util.TruncateDecimal((partido.total / total) * 100, 4);
                    }
                    distrito.tablaVotosPartido.AddRange(vt);
                }

                
                var actas = ActasDetalleDistrito(item.id, item.Nombre, item.TotalVotos.Value, total, ultimo_corte);
                distrito.tablaActasDistrito.AddRange(actas);

                lista.Add(distrito);

            }

            return lista;
        }

        public List<DiputacionDistrito2> getVotosPorPartidosYCandidatura(bool ultimo_corte)
        {
            if (dataTable == null || dataTable.Count() == 0)
                return new List<DiputacionDistrito2>();
            var lista = new List<DiputacionDistrito2>();
            var ltDistritosAyuntamiento = (from row in dataTable
                                           group row by
                                             row["NUMAMBITO_ID"]
                      into g
                                           select
                                    new ClaseGeneral
                                    {
                                        id = int.Parse(g.FirstOrDefault()["NUMAMBITO_ID"].ToString()),
                                        Nombre = "" + g.FirstOrDefault()["NUMAMBITO_NOMBRE"]
                                    }).ToList();

            foreach (var item in ltDistritosAyuntamiento)
            {
                var distrito = new DiputacionDistrito2() { id = item.id, Nombre = item.Nombre, TotalVotos = item.TotalVotos };

                var ltPartidos = from row in dataTable
                                 where int.Parse(row["NUMAMBITO_ID"].ToString()) == item.id
                                 group row by
new { TipoCandidatura_ID = row["TipoCandidatura_ID"], ClavePartido = row["IntegracionVotacion_Clave"] }
                       into g
                                 select
                          new DistribucionVotos
                          {
                              Nombre = "" + g.FirstOrDefault()["IntegracionVotacion_Nombre"],
                              Logo = g.FirstOrDefault()["NombreLogoEmblema"].Equals("CIND")? (g.FirstOrDefault()["NombreLogoEmblema"].ToString()+ g.FirstOrDefault()["Persona_ID"]+".png") : Util.ValidaLogo(g.FirstOrDefault()["NombreLogoEmblema"]),
                              Partido = g.Where(sum => sum["VOTOS"] != null).Sum(r => decimal.Parse(r["VOTOS"].ToString())),
                              Coalicion = g.Sum(f => !(f["VOTOSXCOALICION"] == null) ? decimal.Parse(f["VOTOSXCOALICION"].ToString()) : 0)
                          };

                List<DistribucionVotos> vt = ltPartidos.Count() > 0 ? ltPartidos.ToList() : null;

                var total = ltPartidos.Sum(s => s.Total);
                item.TotalVotos = total;
                distrito.tablaVotosPartido.AddRange(vt);
                var actas = ActasDetalleDistrito(item.id, item.Nombre, item.TotalVotos.Value, total, ultimo_corte);
                distrito.tablaActasDistrito.AddRange(actas);

                lista.Add(distrito);

            }

            return lista;
        }

        internal List<Actas> ActasDetalleDistrito(int idDistrito, string nombre, decimal totalVotosDistrito, decimal totalVotosPartido,bool ultimo_corte, bool agregar = false)
        {         

            var actas = new List<Actas>();
            var ac = new Actas();
            decimal tv = 0;
            var casilla = dataTableSeccion.Where(x => int.Parse(x["AMBITO_ID"].ToString()) == idDistrito);
            ac.id = idDistrito;
            ac.Nombre = nombre;
            ac.Esperadas = casilla.Count();
            ac.Capturadas = casilla.Where(x => int.Parse(x["RegistroActa_Publicacion"].ToString()) == 1).Count();
            ac.Contabilizadas = casilla.Where(x => x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1).Count();
            if (ultimo_corte)
            {
                decimal TotalListaNominal = casilla.Where(x => (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)).Sum(s => int.Parse(s["LISTA_NOMINAL"].ToString()));
                ac.Participacion_Ciudadana = TotalListaNominal == 0 ? 0 : Util.TruncateDecimal(((totalVotosPartido / TotalListaNominal) * 100),4);
                ac.TotalListaNominal = TotalListaNominal;
            }
            else {
                decimal TotalListaNominal = casilla.Where(x => ((x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1) &&
                                           (
                                           int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                           int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                           int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                           )
                                           ))
                                     .Sum(s => decimal.Parse(s["LISTA_NOMINAL"].ToString()));

               tv = casilla.Where(x => (x["TOTAL_VOTOS_CALCULADO"] != null &&
                                                (x["CONTABILIZADA"] != null && int.Parse(x["CONTABILIZADA"].ToString()) == 1)) &&
                                                (
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 1 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 2 ||
                                                int.Parse(x["TIPO_CASILLA_ID"].ToString()) == 3
                                                )
                                               )
                                               .Sum(x => x["TOTAL_VOTOS_CALCULADO"] == null ? 0 : int.Parse(x["TOTAL_VOTOS_CALCULADO"].ToString()));

                ac.Participacion_Ciudadana = TotalListaNominal == 0 ? 0 : Util.TruncateDecimal(((tv / TotalListaNominal) * 100),4);
                ac.TotalListaNominal = TotalListaNominal;
            }

            if (agregar)
                actas.Add(ac);


            var total = new Actas();
            total.id = -1;
            total.Nombre = "Total";
            total.Esperadas = ac.Esperadas;
            total.Capturadas = ac.Capturadas;
            total.Contabilizadas = ac.Contabilizadas;
            total.TotalListaNominal = ac.TotalListaNominal;
            var totalVotacion = ultimo_corte? totalVotosDistrito: tv;//lDistritos.Sum(t => t.TotalVotos);
            if(totalVotacion >0 && total.TotalListaNominal>0)
            total.Participacion_Ciudadana = Util.TruncateDecimal(((totalVotacion / total.TotalListaNominal) * 100), 4);

            actas.Add(total);
            var porcentaje = new Actas();
            porcentaje.id = 0;
            porcentaje.Nombre = "Porcentaje";
            porcentaje.Esperadas = total.TotalListaNominal>0?100:0;
            if(total.Capturadas >0 && total.Esperadas>0)
            porcentaje.Capturadas = Util.TruncateDecimal((total.Capturadas / total.Esperadas) * 100, 4);
            if(total.Contabilizadas>0 && total.Esperadas>0)
            porcentaje.Contabilizadas = Util.TruncateDecimal((total.Contabilizadas / total.Esperadas) * 100, 4);
            //porcentaje.Participacion_Ciudadana = 0;
            //porcentaje.TotalListaNominal = 0;
            actas.Add(porcentaje);
            return actas.OrderBy(x => x.id).ToList();
        }

        internal List<ClaseGeneral> SeccionListaDistrito()
        {
            if (dataTableSeccion == null || dataTableSeccion.Count() == 0)
                return new List<ClaseGeneral>();
            var ltDistritos = from row in dataTableSeccion
                              group row by
                              row["AMBITO_ID"]
                        into g
                              select
                       new ClaseGeneral
                       {
                           id = int.Parse(g.FirstOrDefault()["AMBITO_ID"].ToString()),
                           Nombre = "" + g.FirstOrDefault()["AMBITO_NOMBRE"],
                       };
            return ltDistritos.ToList();

        }
             

        public List<Seccion> Secciones()
        {
            if (dataTableSeccion == null || dataTableSeccion.Count() == 0)
                return new List<Seccion>();
            var ltsecciones = from row in dataTableSeccion
                              where int.Parse("" + row["PrincipioEleccion_ID"]) == 1
                              group row by
                              row["SECCION"]
                        into g
                              select
                       new Seccion
                       {
                           distrito = g.FirstOrDefault()["AMBITO_ID"].ToString(),
                           id = "" + g.FirstOrDefault()["SECCION"],
                           Nombre = "Sección " + g.FirstOrDefault()["SECCION"],
                       };
            return ltsecciones.ToList();
        }

        public object Actas()
        {
            var resultado = from row in dataTableSeccion
                                //where int.Parse(row["TipoCasilla_ID"].ToString()) != 4
                            select new
                            {                               
                                Municipio = row["AMBITO_ID"],
                                Seccion = row["SECCION"] + "",//int.Parse(row["SECCION"] + "").ToString("D4"),
                                Casilla_Descripcion = row["Casilla_Descripcion"],
                                Capturadas = row["RegistroActa_Publicacion"],
                                Contabilizada = row["CONTABILIZADA"],
                                ListaNominal = (row["CONTABILIZADA"] != null && int.Parse(row["CONTABILIZADA"] + "") == 1) ? row["LISTA_NOMINAL"] : 0
                            };
            return resultado.ToList();
        }
    }
}
