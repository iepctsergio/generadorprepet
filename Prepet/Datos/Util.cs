﻿using Prepet.Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prepet.Datos
{
    public static class Util
    {
        //Concatena a los logos de los partidos, la extencion.png y los elementos de la votación establece en vacío
        private static string extensions=".png";
        public static string CNR = "CANDIDATOS/AS NO REGISTRADOS/AS";
        public static string VN = "VOTOS NULOS";

        public static string LogoEstructura(object logo)
        {
            string valLogo = "";
            if (logo != null)
            {
                valLogo = logo.ToString();
                if (valLogo.Equals("VN_") || valLogo.Equals("VN"))
                    valLogo = string.Empty;
                else if (valLogo.Equals("CNR_") || valLogo.Equals("CNR"))
                    valLogo = string.Empty;
                else
                    valLogo = ""+logo;
            }
            return valLogo;
        }

        public static string ValidaLogo(object logo)
        {
            string valLogo = "";
            if (logo != null)
            {
                valLogo = logo.ToString();
                if (valLogo.Equals("VN_") || valLogo.Equals("VN"))
                    valLogo = string.Empty;
                else if (valLogo.Equals("CNR_")|| valLogo.Equals("CNR"))
                    valLogo = string.Empty;
                else
                    valLogo = valLogo + extensions;
            }
            return valLogo;
        }
        //Cambia los nombre de las etiquetas VT: Votos Núlos CNR:Candidatos No Registrato
        public static string ValidaNombre(object nombreCandidato, object logo)
        {
            string val = "" + nombreCandidato;
            if (logo != null)
            {
                if (logo.ToString().Equals("VN_") || logo.ToString().Equals("VN"))
                    val = VN;
                else if (logo.ToString().Equals("CNR_") || logo.ToString().Equals("CNR"))
                    val = CNR;//"CANDIDATOS/AS NO REGISTRADOS/AS";
            }
            return val;
        }
        //OBTIENE LOS VOTOS POR PARTIDOS EN LA COLUMNA: VOTOS
        public static List<Partidos> getVotos(List<Dictionary<string, object>> list)
        {
            if (list == null || list.Count() == 0)
                return new List<Partidos>();
            string[] valores = { };
            var vtPartido = new List<Partidos>();
            int itera = 0;
            foreach (var item in list)
            {
                if (item["VOTACIONESXPARTIDO"] != null)
                {
                    var partidos = item["IntegracionVotacion_Clave"].ToString().Split('_');
                    valores = item["VOTACIONESXPARTIDO"].ToString().Split(',');
                    for (int i = 0; i < valores.Count(); i++)
                    {
                        int valor = 0;
                        if (itera == 0)
                        {
                            var p = new Partidos();
                            p.NombrePartido = partidos[i].Equals("CIND")? partidos[i]+item["Persona_ID"]+extensions: partidos[i] + extensions;
                            if (int.TryParse(valores[i], out valor))
                                p.votos = valor;
                            vtPartido.Add(p);
                        }
                        else
                        {
                            if (int.TryParse(valores[i], out valor))
                                vtPartido[i].votos = vtPartido[i].votos + valor;
                        }
                    }
                    itera++;
                }
                else if (item["VOTOS"] != null)
                {
                    int valor = 0;
                    if (itera == 0)
                    {
                        var p = new Partidos();
                        p.NombrePartido = item["IntegracionVotacion_Clave"].Equals("CIND") ? item["IntegracionVotacion_Clave"].ToString() + item["Persona_ID"] + extensions : item["IntegracionVotacion_Clave"].ToString() + extensions;
                        if (item["IntegracionVotacion_Clave"].ToString().Equals("VN"))
                        {
                            p.NombrePartido = string.Empty;
                            p.Otros = VN; //"VOTOS NULOS";
                        }
                        else if (item["IntegracionVotacion_Clave"].ToString().Equals("CNR"))
                        {
                            p.NombrePartido = string.Empty;
                            p.Otros = CNR; //"CANDIDATOS NO REGISTRADOS";
                        }

                        if (int.TryParse(item["VOTOS"].ToString(), out valor))
                            p.votos = valor;
                        vtPartido.Add(p);
                    }
                    else
                    {
                        if (int.TryParse(item["VOTOS"].ToString(), out valor))
                            vtPartido[0].votos = vtPartido[0].votos + valor;
                    }
                    itera++;
                }
            }
            return vtPartido;
        }

        //  Obtiene los votos por partidos columna:EleccionConcepto_Valor
        internal static List<Partidos> getVotosXPartido(List<Dictionary<string, object>> list)
        {
            if (list == null || list.Count() == 0)
                return new List<Partidos>();
            var vtPartido = new List<Partidos>();
            foreach (var item in list)
            {
                if (item["IntegracionVotacion_Clave"].ToString().Equals("VT"))
                    continue;

                int valor = 0;
                var p = new Partidos();
                p.NombrePartido = item["IntegracionVotacion_Clave"].ToString() + extensions;
                if (item["IntegracionVotacion_Clave"].ToString().Equals("VN"))
                {
                    p.NombrePartido = string.Empty;
                    p.Otros = VN; //"VOTOS NULOS";
                }
                else if (item["IntegracionVotacion_Clave"].ToString().Equals("CNR"))
                {
                    p.NombrePartido = string.Empty;
                    p.Otros = CNR;// "CANDIDATOS NO REGISTRADOS";
                }

                if (int.TryParse(item["EleccionConcepto_Valor"].ToString(), out valor))
                    p.votos = valor;
                else
                    p.votos = valor;
                vtPartido.Add(p);
            }

            return vtPartido;
        }

        //Obtiene los votos por coalición
        internal static List<Partidos> getVotos2(object votos)
        {
            string[] array = { };
            if (votos == null)
                return new List<Partidos>();

            array = votos.ToString().Split(',');
            var vtPartido = new List<Partidos>();
            for (int i = 0; i < array.Length; i++)
            {
                int valor = 0;
                var p = new Partidos();
                if (int.TryParse(array[i], out valor))
                    p.votos = valor;
                vtPartido.Add(p);
            }
            return new List<Partidos>();
        }

        //Contatene la extencion .png de las imagenes de las personas
        public static string getIdPersona(List<Dictionary<string, object>> list, int i)
        {
            string idPersona = i == 1 ? "default.png" : "0";
            if (list != null)
            {
                string key = "";
                foreach (var item in list)
                {
                    int valor = 0;
                    if (item.ContainsKey("RegistroCandidato_Candidato_Persona_ID"))
                    {
                        key = "RegistroCandidato_Candidato_Persona_ID";
                    }
                    else if (item.ContainsKey("Candidato_Persona_ID"))
                    {
                        key = "Candidato_Persona_ID";
                    }
                    else if (item.ContainsKey("Persona_ID"))
                    {
                        key = "Persona_ID";
                    }
                    
                    if (!string.IsNullOrEmpty(key))
                    {
                        if (item[key] != null)
                        {
                            if (int.TryParse(item[key].ToString(), out valor))
                                if (valor > 0)
                                {
                                    idPersona = i == 1 ? valor + extensions : "" + valor;
                                    return idPersona;
                                }
                        }
                    }
                }
            }
            return idPersona;
        }

        internal static List<VotacionX> VotosXDistritos(List<Dictionary<string, object>> list)
        {
            var votaciones = new List<VotacionX>();
            foreach (var item in list)
            {
                int valor = 0;
                if (int.TryParse("" + item["LUGARCANDIDATURA"], out valor))
                {
                    if (valor == 1)
                    {
                        var dis = new VotacionX();
                        dis.id = int.Parse("" + item["NUMAMBITO_ID"]);
                        dis.Nombre = "" + item["NUMAMBITO_NOMBRE"];
                        dis.votos = int.Parse("" + item["VOTOS"]);
                        votaciones.Add(dis);
                    }
                }
            }
            return votaciones;
        }


        public static decimal TruncateDecimal(decimal value, int precision)
        {
            decimal step = (decimal)Math.Pow(10, precision);
            decimal tmp = Math.Truncate(step * value);
            return tmp / step;
        }


        //Busca de una lista todos los distintos
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
        (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }
}
