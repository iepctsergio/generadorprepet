﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Prepet.Conexion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prepet.Model;
using Prepet.Model.Entidad;
using System.Net;
using System.Threading;
using Newtonsoft.Json.Linq;
using Prepet.Datos;
using System.Data;

namespace Prepet
{
    public class Main
    {
        private string ProcesoOrdinario = "ORD-2018";
        private int idAmbito = 0;//Todos los ambitos
        private DateTime DateTimeFecha;
        /// <summary>
        /// Crea los archivos Json de la publicación
        /// </summary>
        /// <param name="timer_update">Tiempo en que la página de publicación se debe actualizar</param>
        /// <param name="ultimo_corte">Indicador del último corte</param>
        public string GenerarJsonGrupo(int timer_update = 60000,bool ultimo_corte=false)
        {
            var fecha_publicacion = new ConexionBD().HoraCorte();
            var HoraCorte = fecha_publicacion[0];
            var array = fecha_publicacion[1].Split('/');
            DateTimeFecha = new DateTime(int.Parse(array[2]), int.Parse(array[1]), int.Parse(array[0]));

            DataTable Gob_SeccionCasilla = new ConexionBD().ejecutarQuery("call PREPET_spDesgloseXCasilla('" + ProcesoOrdinario + "', " + TipoEleccion.Gubernatura.GetHashCode() + ", " + 0 + ")");
            var Gob_TableCandidatura = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXCandidatura('" + ProcesoOrdinario + "'," + TipoEleccion.Gubernatura.GetHashCode() + "," + idAmbito + ",1)");
            var Gob_DataTable = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXPartido('" + ProcesoOrdinario + "'," + TipoEleccion.Gubernatura.GetHashCode() + "," + idAmbito + ",1)");

            DataTable Dip_SeccionCasilla = new ConexionBD().ejecutarQuery("call PREPET_spDesgloseXCasilla('" + ProcesoOrdinario + "', " + TipoEleccion.Diputaciones.GetHashCode() + ", " + 0 + ")");
            var Dip_Loc_TableCandidatura = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXCandidatura('" + ProcesoOrdinario + "'," + TipoEleccion.Diputaciones.GetHashCode() + "," + idAmbito + ",1)");
            var Dip_Loc_DataTable = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXPartido('" + ProcesoOrdinario + "'," + TipoEleccion.Diputaciones.GetHashCode() + "," + idAmbito + ",1)");

            DataTable Mun_SeccionCasilla = new ConexionBD().ejecutarQuery("call PREPET_spDesgloseXCasilla('" + ProcesoOrdinario + "', " + TipoEleccion.Municipales.GetHashCode() + ", " + 0 + ")");
            var Mun_TableCandidatura = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXCandidatura('" + ProcesoOrdinario + "'," + TipoEleccion.Municipales.GetHashCode() + "," + idAmbito + ",1)");
            var Mun_DataTable = new ConexionBD().IEnumerableCollectionData("call Siee_PREPET_spVotacionXPartido('" + ProcesoOrdinario + "'," + TipoEleccion.Municipales.GetHashCode() + "," + idAmbito + ",1)");

            string archivo = new Prepet.MainCSV().CreateCSVFile(Gob_SeccionCasilla.Copy(), Dip_SeccionCasilla.Copy(), Mun_SeccionCasilla.Copy(), fecha_publicacion, ultimo_corte);

            Thread thread1 = new Thread(() => JsonGubernatura(Gob_SeccionCasilla, Gob_TableCandidatura, Gob_DataTable, timer_update, HoraCorte, ultimo_corte));

            Thread thread2 = new Thread(() => JsonDiputaciones(Dip_SeccionCasilla, Dip_Loc_TableCandidatura, Dip_Loc_DataTable, timer_update, HoraCorte, ultimo_corte));

            Thread thread3 = new Thread(() => JsonMunicipiosLocalidad(Mun_SeccionCasilla, Mun_TableCandidatura, Mun_DataTable, timer_update, HoraCorte, ultimo_corte));
            
            thread1.Start();
            thread2.Start();
            thread3.Start();
            
           //while (thread3.IsAlive)
           // {

            //}


            while (thread1.IsAlive || thread2.IsAlive||thread3.IsAlive)
            {

            }
            return archivo;
           
        }


        public void JsonGubernatura(DataTable rstable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable, int timer_update,string HoraCorte,bool ultimo_corte) {
            Data grupoGoberntura = new Data
            {
                gobernatura = ProcesaGoberatura(rstable,dataTableCandidatura,dataTable, timer_update, HoraCorte, ultimo_corte)
            };
            Dictionary<string, object> dic_entidad = new Dictionary<string, object>();
            dic_entidad.Add("informacionGral", grupoGoberntura.gobernatura.informacionGral);
            dic_entidad.Add("entidad", grupoGoberntura.gobernatura.entidad);
            GuardarJsonData(@"c:\Json\Data\GOB_ENTIDAD.json", dic_entidad);

            Dictionary<string, object> dic_detalle = new Dictionary<string, object>();
            dic_detalle.Add("informacionGral", grupoGoberntura.gobernatura.informacionGral);
            dic_detalle.Add("detalle_entidad", grupoGoberntura.gobernatura.detalleEntidad);
            GuardarJsonData(@"c:\Json\Data\GOB_DET_ENTIDAD.json", dic_detalle);

            Dictionary<string, object> distrito = new Dictionary<string, object>();
            distrito.Add("informacionGral", grupoGoberntura.gobernatura.informacionGral);
            distrito.Add("distritos", grupoGoberntura.gobernatura.distritos);
            GuardarJsonData(@"c:\Json\Data\GOB_DISTRITO.json", distrito);

        
            Dictionary<string, object> seccion = new Dictionary<string, object>();
            seccion.Add("informacionGral", grupoGoberntura.gobernatura.informacionGral);
            seccion.Add("seccion", grupoGoberntura.gobernatura.seccion);
            GuardarJsonData(@"c:\Json\Data\GOB_SECCION.json", seccion);
            grupoGoberntura = null;
        }

        public void JsonDiputaciones(DataTable rstable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable, int timer_update, string HoraCorte,bool ultimo_corte)
        {
            Data grupo_dip = new Data
            {
                diputaciones = ProcesaDiputaciones(rstable,dataTableCandidatura, dataTable, timer_update, HoraCorte, ultimo_corte),
            };

            Dictionary<string, object> dip_entidad = new Dictionary<string, object>();
            Dictionary<string, object> infoGeneral = new Dictionary<string, object>();
            infoGeneral.Add("informacionGralDistrito", grupo_dip.diputaciones.informacionGralDiputacion);
            infoGeneral.Add("informacionGralPartidos", grupo_dip.diputaciones.informacionGralPartidos);

            dip_entidad.Add("informacionGral", infoGeneral);
            dip_entidad.Add("entidad", grupo_dip.diputaciones.entidad);
            GuardarJsonData(@"c:\Json\Data\DIP_ENTIDAD.json", dip_entidad);

            Dictionary<string, object> dip_detalle = new Dictionary<string, object>();
            dip_detalle.Add("informacionGral", infoGeneral);
            dip_detalle.Add("detalle_entidad", grupo_dip.diputaciones.detalle);
            GuardarJsonData(@"c:\Json\Data\DIP_DET_ENTIDAD.json", dip_detalle);

            Dictionary<string, object> dip_distrito = new Dictionary<string, object>();
            dip_distrito.Add("informacionGral", infoGeneral);
            dip_distrito.Add("distritos", grupo_dip.diputaciones.detalleDistrito);
            GuardarJsonData(@"c:\Json\Data\DIP_DISTRITO.json", dip_distrito);

            Dictionary<string, object> dipseccion = new Dictionary<string, object>();
            dipseccion.Add("informacionGral", grupo_dip.diputaciones.informacionGralDiputacion);
            dipseccion.Add("seccion", grupo_dip.diputaciones.seccion);
            GuardarJsonData(@"c:\Json\Data\DIP_SECCION.json", dipseccion);
            grupo_dip = null;
        }

        public void JsonMunicipiosLocalidad(DataTable rstable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable, int timer_update,string HoraCorte,bool ultimo_corte) {
            Data grupo_pres_mun_reg = new Data
            {
                ayuntamientos = ProcesaAyuntamientos(rstable,dataTableCandidatura,dataTable,timer_update, HoraCorte, ultimo_corte)
            };

            Dictionary<string, object> mun_entidad = new Dictionary<string, object>();
            mun_entidad.Add("informacionGral", grupo_pres_mun_reg.ayuntamientos.informacionGral);
            mun_entidad.Add("entidad", grupo_pres_mun_reg.ayuntamientos.entidad);
            GuardarJsonData(@"c:\Json\Data\MUN_ENTIDAD.json", mun_entidad);

            Dictionary<string, object> mun_detalle = new Dictionary<string, object>();
            mun_detalle.Add("informacionGral", grupo_pres_mun_reg.ayuntamientos.informacionGral);
            mun_detalle.Add("municipios", grupo_pres_mun_reg.ayuntamientos.detalle);
            GuardarJsonData(@"c:\Json\Data\MUNICIPIOS.json", mun_detalle);

            Dictionary<string, object> mun_distrito = new Dictionary<string, object>();
            mun_distrito.Add("informacionGral", grupo_pres_mun_reg.ayuntamientos.informacionGral);
            mun_distrito.Add("detalle_municipio", grupo_pres_mun_reg.ayuntamientos.detalle_municipios);
            GuardarJsonData(@"c:\Json\Data\MUN_DETALLE.json", mun_distrito);

            Dictionary<string, object> munseccion = new Dictionary<string, object>();
            munseccion.Add("informacionGral", grupo_pres_mun_reg.ayuntamientos.informacionGral);
            munseccion.Add("seccion", grupo_pres_mun_reg.ayuntamientos.seccion);
            GuardarJsonData(@"c:\Json\Data\MUN_SECCION.json", munseccion);
            grupo_pres_mun_reg = null;
        }


        #region CREA 3 ARCHIVOS JSON       
        public gobernatura ProcesaGoberatura(DataTable rstable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable,int timer_update, string HoraCorte,bool ultimo_corte) {

            var extraccionDato = new Datos.ExtraccionDatoGobernatura();// ProcesoOrdinario, TipoEleccion.Gubernatura.GetHashCode(), idAmbito, ultimo_corte);
            extraccionDato.DataTableSeccionCasilla(rstable,dataTableCandidatura,dataTable);
            var gob_entidad = new GobernaturaEntidad();
            //Gubernatura -Entidad / Votos por Candidatura
             #region  Gubernatura Entidad
             var informacion_general_gob= extraccionDato.InformacionGenerales(HoraCorte, ultimo_corte);
             informacion_general_gob.timer_update = timer_update;
             informacion_general_gob.fechaCorte = DateTimeFecha.ToString("dd 'de' MMMM 'de' yyyy");
             gob_entidad.votosCandidatura.tablaVotosCandidato = extraccionDato.VotosCandidatura();
             gob_entidad.votosCandidatura.totalVotos = gob_entidad.votosCandidatura.tablaVotosCandidato.Sum(x => x.totalVotos);
             gob_entidad.votosCandidatura.candidatos = extraccionDato.VotosXPartidosYCandidatura(gob_entidad.votosCandidatura.totalVotos);


             //Gubernatura -Entidad / Votos por partido politico y candidatura independiente
             gob_entidad.votosCandidaturaIndependiente.tablaVotosCandidato = extraccionDato.VotosPartidoCandidaturaIndependiente();
             gob_entidad.votosCandidaturaIndependiente.totalVotos = gob_entidad.votosCandidaturaIndependiente.tablaVotosCandidato.Sum(x => x.totalVotos);
            #endregion

             #region Gubernatura Detalle-Entidad
             ModuloDetalleEntidad detalleEntidad = new ModuloDetalleEntidad();
             
             //Gubernatura -Detalle\Entidad / Votos por Candidatura
             detalleEntidad.votosCandidatura.tablaVotosCandidato = gob_entidad.votosCandidatura.tablaVotosCandidato;
             detalleEntidad.votosCandidatura.totalVotos = gob_entidad.votosCandidatura.tablaVotosCandidato.Sum(x => x.totalVotos);
             detalleEntidad.votosCandidatura.tablaVotosPartido = extraccionDato.DetalleEntidadVotosCandidatura(detalleEntidad.votosCandidatura.totalVotos);
             detalleEntidad.votosCandidatura.tablaActasDistrito = extraccionDato.ActasDetalleXDistrito(detalleEntidad.votosCandidatura.totalVotos,ultimo_corte);

            //Gubernatura -Detalle\Entidad /Votos por partido politico y candidatura independiente
            detalleEntidad.votosCandidaturaIndependiente.tablaVotosCandidato = gob_entidad.votosCandidaturaIndependiente.tablaVotosCandidato;
             detalleEntidad.votosCandidaturaIndependiente.totalVotos = detalleEntidad.votosCandidaturaIndependiente.tablaVotosCandidato.Sum(x => x.totalVotos);
             detalleEntidad.votosCandidaturaIndependiente.tablaActasDistrito = extraccionDato.ActasDetalleXDistrito(detalleEntidad.votosCandidaturaIndependiente.totalVotos, ultimo_corte);
             detalleEntidad.votosCandidaturaIndependiente.tablaVotosPartido = extraccionDato.DetalleEntidad();

             #endregion

             #region Gubernatura Distrito
             ModuloDistrito distrito = new ModuloDistrito();
             distrito.votosCandidatura.tablaVotosCandidato= gob_entidad.votosCandidatura.tablaVotosCandidato;
             distrito.votosCandidatura.tablaDistrito = extraccionDato.DistritosGobenaturaVotosCandidatura();
             distrito.votosCandidatura.tablaActasDistrito = extraccionDato.ActasXDistrito(distrito.votosCandidatura.tablaDistrito,ultimo_corte);


             distrito.votosCandidaturaIndependiente.tablaDistrito = extraccionDato.DistritosVPCandidaturaIndependienteTabla();
             distrito.votosCandidaturaIndependiente.tablaVotosCandidato = gob_entidad.votosCandidaturaIndependiente.tablaVotosCandidato;
             distrito.votosCandidaturaIndependiente.totalVotos = distrito.votosCandidaturaIndependiente.tablaVotosCandidato.Sum(x => x.totalVotos);
             distrito.votosCandidaturaIndependiente.tablaActasDistrito = extraccionDato.ActasXDistrito(distrito.votosCandidaturaIndependiente.tablaDistrito,ultimo_corte);
             #endregion
            
            #region Gubernatura Desglose por Seccion
            ModuloSeccion seccion = new ModuloSeccion();
            seccion.ltDistritos = extraccionDato.SeccionListaDistrito();
            //seccion.Partidos = extraccionDato.Partidos();
            seccion.ltSecciones = extraccionDato.Secciones();
            seccion.Dist_Sec_Casilla = extraccionDato.dataTableSeccion;
            seccion.Actas = extraccionDato.Actas();
            extraccionDato = null;                   


            gobernatura gobernatura = new gobernatura
            {
                informacionGral= informacion_general_gob,
                entidad = gob_entidad,
                distritos = distrito,
                detalleEntidad = detalleEntidad,
                seccion = seccion

            };
            return gobernatura;
        }

        //ORD-2018,  1  , 0
        public Diputaciones ProcesaDiputaciones(DataTable rstable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable, int timer_update, string HoraCorte,bool ultimo_corte) {

            var extraccionDato = new Datos.ExtraccionDatoDiputacion();//ProcesoOrdinario, TipoEleccion.Diputaciones.GetHashCode(), idAmbito, ultimo_corte);
            extraccionDato.DataTableSeccionCasilla(rstable, dataTableCandidatura, dataTable);
            var dip_entidad = new DiputacionesEntidad();
            //Diputaciones Distritos
            var informacion_general= extraccionDato.InformacionGenerales(HoraCorte, ultimo_corte);
            informacion_general.timer_update = timer_update;
            informacion_general.fechaCorte = DateTimeFecha.ToString("dd 'de' MMMM 'de' yyyy");
            var informacion_general_partido = extraccionDato.InformacionGeneralesPartidos(HoraCorte, ultimo_corte);
            informacion_general_partido.timer_update = timer_update;
            informacion_general_partido.fechaCorte = DateTimeFecha.ToString("dd 'de' MMMM 'de' yyyy");
            dip_entidad.votosCandidatura.tablaVotosCandidato = extraccionDato.VotosDiputacionDistrito();
            dip_entidad.votosCandidatura.totalVotos = dip_entidad.votosCandidatura.tablaVotosCandidato.Sum(x => x.totalVotos);
            //Diputaciones Locales / Votos por partido politico y candidatura independiente
            dip_entidad.votosCandidaturaIndependiente.tablaVotosCandidato = extraccionDato.VotosPartidoCandidaturaIndependiente();
            dip_entidad.votosCandidaturaIndependiente.totalVotos = dip_entidad.votosCandidaturaIndependiente.tablaVotosCandidato.Sum(x => x.totalVotos);


            var dip_detalle_entidad = new DiputacionDetalleEntidad();
            //Diputacion Detalle por distrito
            dip_detalle_entidad.votosCandidatura.tablaVotosCandidato = dip_entidad.votosCandidatura.tablaVotosCandidato;
            dip_detalle_entidad.votosCandidatura.tablaDistrito = extraccionDato.DistritosDiputacionesCandidatura();
            dip_detalle_entidad.votosCandidatura.tablaActasDistrito = extraccionDato.ActasXDistrito(dip_detalle_entidad.votosCandidatura.tablaDistrito,ultimo_corte,1);

            //Diputacion /Votacion por partido politico y candidatura independiente
            dip_detalle_entidad.votosCandidaturaIndependiente.tablaVotosCandidato = dip_entidad.votosCandidaturaIndependiente.tablaVotosCandidato;
            dip_detalle_entidad.votosCandidaturaIndependiente.totalVotos = dip_entidad.votosCandidaturaIndependiente.totalVotos;
            dip_detalle_entidad.votosCandidaturaIndependiente.tablaDistrito = extraccionDato.DistritosDiputacionesPartidoYcandidaturaIndependiente();
            dip_detalle_entidad.votosCandidaturaIndependiente.tablaActasPartido = extraccionDato.ActasXDistrito(dip_detalle_entidad.votosCandidatura.tablaDistrito, ultimo_corte, 2);


            var dip_detalle_distrito = new DiputacionDetalleDistrito();
            dip_detalle_distrito.votosCandidatura.listaDetalleDistrito = extraccionDato.getVotosPorPartidos(ultimo_corte);
            dip_detalle_distrito.votosCandidaturaIndependiente.listaDetalleDistrito = extraccionDato.getVotosPorPartidosYCandidatura(ultimo_corte);

            #region Diputacion Desglose por Seccion
            ModuloSeccion seccion = new ModuloSeccion();
            seccion.ltDistritos = extraccionDato.SeccionListaDistrito();
            //seccion.Partidos = extraccionDato.Partidos();
            seccion.ltSecciones = extraccionDato.Secciones();
            seccion.Dist_Sec_Casilla = extraccionDato.dataTableSeccion;
            seccion.Actas = extraccionDato.Actas();
            extraccionDato = null;
            #endregion

            Diputaciones diputaciones = new Diputaciones
            {
                informacionGralDiputacion = informacion_general,
                informacionGralPartidos = informacion_general_partido,
                entidad = dip_entidad,
                detalle = dip_detalle_entidad,
                detalleDistrito = dip_detalle_distrito,
                seccion = seccion,
               // casilla = casilla
            };

            return diputaciones;
        }

        //ORD-2018,  3  , 0
        public Ayuntamiento ProcesaAyuntamientos(DataTable rsTable, IEnumerable<Dictionary<string, object>> dataTableCandidatura, IEnumerable<Dictionary<string, object>> dataTable, int timer_update, string HoraCorte,bool ultimo_corte)
        {
            var extraccionDato = new Datos.ExtraccionDatoAyuntamiento();// ProcesoOrdinario, TipoEleccion.Municipales.GetHashCode(), idAmbito, ultimo_corte);
            extraccionDato.DataTableSeccionCasilla(rsTable, dataTableCandidatura, dataTable);
            //Presidencia Municipal y Regidurias
            var ayuntamiento = new AyuntamientoEntidad();
            var informacion_general= extraccionDato.InformacionGenerales(HoraCorte, ultimo_corte);
            informacion_general.timer_update = timer_update;
            informacion_general.fechaCorte = DateTimeFecha.ToString("dd 'de' MMMM 'de' yyyy");

            //Diputaciones Municipios

            ayuntamiento.votosCandidatura.tablaVotosCandidato = extraccionDato.VotosAyuntamientoDistrito();
            ayuntamiento.votosCandidatura.totalVotos = ayuntamiento.votosCandidatura.tablaVotosCandidato.Sum(x => x.totalVotos);
            //Diputaciones Locales / Votos por partido politico y candidatura independiente
            ayuntamiento.votosCandidaturaIndependiente.tablaVotosCandidato = extraccionDato.VotosPartidoCandidaturaIndependiente();
            ayuntamiento.votosCandidaturaIndependiente.totalVotos = ayuntamiento.votosCandidaturaIndependiente.tablaVotosCandidato.Sum(x => x.totalVotos);

            var detalle_entidad = new AyuntamientoDetalleEntidad();
            //Diputacion Detalle por Municipios
            detalle_entidad.votosCandidatura.tablaVotosCandidato = ayuntamiento.votosCandidatura.tablaVotosCandidato;
            detalle_entidad.votosCandidatura.tablaDistrito = extraccionDato.DistritosAyuntamientoCandidatura();
            detalle_entidad.votosCandidatura.tablaActasDistrito = extraccionDato.ActasXDistrito(detalle_entidad.votosCandidatura.tablaDistrito,ultimo_corte);
           
          
            detalle_entidad.votosCandidaturaIndependiente.tablaVotosCandidato = ayuntamiento.votosCandidaturaIndependiente.tablaVotosCandidato;
            detalle_entidad.votosCandidaturaIndependiente.totalVotos = ayuntamiento.votosCandidaturaIndependiente.totalVotos;
            detalle_entidad.votosCandidaturaIndependiente.tablaDistrito = extraccionDato.DistritosAyuntamientoPartidoYcandidaturaIndependiente();

            var detalle_municipio = new DetalleMuncipios();
            detalle_municipio.votosCandidatura.listaDetalleMunicipio = extraccionDato.getVotosPorPatidos(ultimo_corte);
            detalle_municipio.votosCandidaturaIndependiente.listaDetalleMunicipio = extraccionDato.getVotosPorPartidosYCandidatura(ultimo_corte);

            ModuloSeccion seccion = new ModuloSeccion();
            seccion.ltDistritos = extraccionDato.SeccionListaDistrito();
            //seccion.Partidos = extraccionDato.Partidos();
            seccion.ltSecciones = extraccionDato.Secciones();
            seccion.Dist_Sec_Casilla = extraccionDato.dataTableSeccion;
            seccion.Actas = extraccionDato.Actas();
            extraccionDato = null;
            #endregion

            Ayuntamiento ayuntamientos = new Ayuntamiento
            {
                informacionGral= informacion_general,
                entidad = ayuntamiento,
                detalle = detalle_entidad,
                detalle_municipios = detalle_municipio,
                seccion = seccion
            };
            return ayuntamientos;
        }
        #endregion

        private void GuardarJsonData(string url, object json)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(url))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, json);
            }
        }
    }
}

