﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using System.Threading;
using System.IO;

namespace RedisCliente
{
    class Program
    {

        static void Main(string[] args)
        {
            Thread hilo = new Thread(new ThreadStart(MyThreadMethod));
            hilo.Start();
        }

        static void MyThreadMethod()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("192.168.10.226:1234");
            string value = "";
            IDatabase db = redis.GetDatabase();
            int valCiclo = 1;
            new Prepet.MainEstructura().GenerarStructura();
            //db.StringSet("PUBLICADOR_SOLICITUD", "");
            //db.StringSet("RESPUESTA_SERVICIO", "");
            //db.StringSet("ARCHIVO_CSV", "");

            //  string hola = new Prepet.Main().GenerarJsonGrupo(50000, false);

            Console.WriteLine(" <" + HoraData.getHora() + "> Generador de Archivos Iniciado");
            Console.WriteLine(" <" + HoraData.getHora() + "> Esperando instruciones.... \n\n");
            while (true)
            {

                string temp = db.StringGet("PUBLICADOR_SOLICITUD");
                if(temp != value && temp.Length>0 && temp != "")
                {
                    value = temp;
                    if (temp.IndexOf("INICIAL_JSON") > -1)
                    {

                        // long 60000
                        Console.WriteLine(" <" + HoraData.getHora() + "> Resolviendo petición tipo: 'Inicial' | Ciclo número: " + (valCiclo));

                        Console.WriteLine(" <" + HoraData.getHora() + "> Generando Archivos JSON");
                        new Prepet.MainEstructura().GenerarStructura();
                        string nombreArchivo = new Prepet.Main().GenerarJsonGrupo(50000, false);
                        Console.WriteLine(" <" + HoraData.getHora() + "> Archivos JSON Creados");
                        
                        Thread.Sleep(1000);
                        db.StringSet("RESPUESTA_SERVICIO", "Proceso_Inicial_" + HoraData.getTiempo());
                        Console.WriteLine(" <" + HoraData.getHora() + "> Resuelto petición tipo: 'Inicial' | Ciclo número: " + (valCiclo++)+"\n");



                    }
                    else if (temp.IndexOf("COMUN_JSON") > -1)
                    {



                        Console.WriteLine(" <"+HoraData.getHora()+"> Resolviendo petición tipo: 'Común' | Ciclo número: " + (valCiclo));

                        Console.WriteLine(" <" + HoraData.getHora() + "> Generando Archivos de Información");
                        string nombreArchivo = new Prepet.Main().GenerarJsonGrupo(50000,false);
                        Console.WriteLine(" <" + HoraData.getHora() + "> Archivos de Información Creados");

                        Thread.Sleep(1000);
                        //db.StringSet("ARCHIVO_CSV", "20180615_1832_BD.tar.gz");
                        db.StringSet("ARCHIVO_CSV", nombreArchivo);
                        db.StringSet("RESPUESTA_SERVICIO", "Proceso_Común_" + HoraData.getTiempo());
                        Console.WriteLine(" <" + HoraData.getHora() + "> Resuelto petición tipo: 'Común' | Ciclo número: " + (valCiclo++) + "\n");



                    }
                    else if (temp.IndexOf("FINAL_JSON") > -1)
                    {


                        Console.WriteLine("Resolviendo petición tipo: 'Final' | Ciclo número: " + (valCiclo) );

                        Console.WriteLine(" <" + HoraData.getHora() + "> Generando Archivos de Información Final");
                        string nombreArchivo = new Prepet.Main().GenerarJsonGrupo(50000, true);
                        Console.WriteLine(" <" + HoraData.getHora() + "> Archivos de Información Final Creados");

                        Thread.Sleep(1000);
                        db.StringSet("ARCHIVO_CSV", nombreArchivo);
                        db.StringSet("RESPUESTA_SERVICIO", "Proceso_Común_" + HoraData.getTiempo());
                        Console.WriteLine(" <" + HoraData.getHora() + "> Resuelto petición tipo: 'Común' | Ciclo número: " + (valCiclo++) + "\n");


                    }
                }
                Thread.Sleep(2000);
            }
        }
    }
}
