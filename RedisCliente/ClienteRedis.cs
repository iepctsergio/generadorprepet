﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace RedisCliente
{
    public class ClienteRedis
    {
        private ConnectionMultiplexer redis;
        IDatabase redisCliente;
        public ClienteRedis(String host, String puerto)
        {
            redis = ConnectionMultiplexer.Connect(host + ":" + puerto);
            redisCliente = redis.GetDatabase();
        }

        public void setValorString(String campo, String valor)
        {
            redisCliente.StringSet(campo, valor);
        }

        public String getValorString(String campo)
        {
            return redisCliente.StringGet(campo);
        }
    }
}
