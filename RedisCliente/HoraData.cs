﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisCliente
{
    class HoraData
    {
        public static String getTiempo()
        {
            var HoraTiempo = DateTime.Now;
            return HoraTiempo.Year + "/" + HoraTiempo.Month + "/" + HoraTiempo.Day 
            + " - " + HoraTiempo.Hour + ":" + ((HoraTiempo.Minute < 10) ? "0" + HoraTiempo.Minute : "" + HoraTiempo.Minute) + ":" 
            + ((HoraTiempo.Second < 10) ? "0" + HoraTiempo.Second : "" + HoraTiempo.Second);
        }

        public static String getHora()
        {
            var HoraTiempo = DateTime.Now;
            return HoraTiempo.Hour + ":" + ((HoraTiempo.Minute < 10) ? "0" + HoraTiempo.Minute : "" + HoraTiempo.Minute) + ":"
               + ((HoraTiempo.Second < 10) ? "0" + HoraTiempo.Second : "" + HoraTiempo.Second);
        }
    }
}
